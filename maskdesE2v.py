
import numpy as np
import matplotlib.pyplot as plt
##from astropy import wcs as pywcs
#import pywcs
#from astrometry.libkd import spherematch as spm
from matplotlib.patches import Rectangle,Circle
import sys,os
from astropy.io import ascii as asciitable

import logging  # only needed for debugging

# import parameters for Ham detectors:
from configE2v import *


#logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
logging.basicConfig(filename='tmp.log', filemode='w', level=logging.DEBUG)

# Ham: totx =3136




dbglvl = 0
#niter=50

#*** need to update
def drawHam(ax):
    # draw predicted Hamamatsu edges relative to E2V (assuming same centres:

    cenXshift=0.0

    rect=Rectangle((0+cenXshift,0),ccd_dx,ccd_dy,color='none',ec='r')
    ax.add_patch(rect)
    rect=Rectangle((0+ccd_dx+ccd_xgap+cenXshift,0),ccd_dx,ccd_dy,color='none',ec='r')
    ax.add_patch(rect)
    rect=Rectangle((0+2.*(ccd_dx+ccd_xgap)+cenXshift,0),ccd_dx,ccd_dy,color='none',ec='r')
    ax.add_patch(rect)

    # GMOS imaging field:
    plt.plot((x0,x0),(y0,y1),'k-',lw=2)
    plt.plot((x0,x1),(y1,y1),'k-',lw=2)
    plt.plot((x1,x1),(y1,y0),'k-',lw=2)
    plt.plot((x1,x0),(y0,y0),'k-',lw=2)


def defineGMOSspec(xobj,yobj,slen,wt,NSmode='micro'):
    # -- Use optical model to obtain wavelength limits (converted to pixels)
    # based on grating and filter selected. 

    # GMOS HAS RED AND BLUE REVERSED FROM DIRECTION GOD INTENDED!!
    from configE2v import *


    if(NSmode=='band'):
        print 'BAND shuffle'
        # band shuffle instead of micro-shuffle:
        slen=slen/2.0
        yHt=ymaxField-yminField
        yF0=yminField+0.33*yHt
        ymaxField=yminField+0.67*yHt
        yminField = yF0

  

    # R150, cenwave=850nm, +RG610:
# Ham#    cenwave=850 ; linDisp=0.39353344971414417 ; coeff=np.array([-7.78878524e-02,   1.54497292e+02])
    # e2v from: ../maskDesTests/MyCode/q.py
#    cenwave=850 ; linDisp=0.35762352242772855 ; coeff=np.array([-0.077888, 170.010872])
#    #cenwave=800 ; linDisp=0.35344379303792917 #; coeff=np.array([-0.050880, -12.860031])
#    #cenwave=750 ; linDisp=0.35402666435139885 ; coeff=np.array([-0.054557, -148.224445])
#
#    
#    blueCutA = 610.0
#    redCutA = 1045.0
    
    #Spectrum Length: 1105.3698238764111 (pixels)
    #(1050.-610.)/linDisp = 1118.
    # w. OG515. Spectrum Length: 1346.7724290907997 (pixels)
    #In [77]: (1050-515.)/0.39353344971414417
    #Out[77]: 1359.4778293652412
    # OKAY, actual red cutoff in gmmps seems to be 1045A rather than 1050, so update.
    
    lenRightPix = (cenwave - blueCutA)/linDisp  # REVERSED FOR GMOS!
    lenLeftPix = (redCutA - cenwave)/linDisp 

#    xcen=3000. #**** made-up!
    xcen=ccd_cx #**** made-up!
#    xspec=xobj #(xobj-xcen)+(-0.2)*(xobj-xcen)+xcen # make up anamorphic factor
#    xspec = xobj -np.polyval(coeff,xobj) # measured anamorphic factor  # REVERSED FOR GMOS!
    xspec = xobj +np.polyval(WLcoeff,xobj) # measured anamorphic factor
    yspec=yobj 

    xlo = lenLeftPix * 2.0 # binning
    xhi = lenRightPix * 2.0 # binning
    

    # -- start off with ylo/yhmaxi half slit length
    yhi=slen/2.
    ylo=slen/2.



    # check for objects where spectra fall out of field:
    keepflag=np.ones(len(xobj))
    outy = np.where( (yobj < yminField) | (yobj>ymaxField) )[0]
    keepflag[outy]=0
    xLeft = xspec-xlo
    xRight = xspec+xhi
    outLeft = np.where(xLeft< xminSField)[0]
    keepflag[outLeft]=0 
    outRight = np.where(xRight> xmaxSField)[0] 
    keepflag[outRight]=0 
    
    # also, object must be visible in field. DOH!
    outFoV=np.where( (xobj<xminField) | (xobj>xmaxField) | (yobj<yminField) | (yobj>ymaxField) )[0]    
    keepflag[outFoV]=0
    sStars = np.where(wt==9.0)[0]
    if(len(sStars)>0):
        keepflag[sStars]=1.0 # keep setup stars regardless. does not matter that their spectra fall off array

#    print len(xLeft),' ',len(xlo)
    axlo = np.repeat(xlo,len(xLeft))
    axhi = np.repeat(xhi,len(xRight))
#    print len(axlo)

    return xspec,yspec,axlo,axhi,ylo,yhi, keepflag


def resolveCollisions(x0,xWidth,y0,yWidth,wt,inMask,ss,xobj,yobj,xpad=0.0,ypad=0.0):
    # coords are lower left and widths of boxes;
    # wt is P(include in mask); inMask is flag for already accepted objects
    
    # Assume inMask=1.0 (/pri>=1.0) objects have already been pre-cleaned for collisions.
    # Loop over list and fit in spectra where possible:
    
    tx0=x0-xpad/2.
    tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
    ty0=y0-ypad/2.
    ty1=y0+yWidth+ypad/2.
    
    if(dbglvl>1):
        figDEBUG = plt.figure()
        axDEBUG = figDEBUG.add_subplot(111)
        xCen = (tx0+tx1)/2.0 ; yCen = (ty0+ty1)/2.0 
        plt.plot(xCen,yCen,'k.')
        plt.plot(xobj,yobj,'ko')
        if (dbglvl>1): 
            for i in range(len(x0)): plt.text(xCen[i],yCen[i],'%.0f'%i)

        # plot slits currently in mask:
        okMask = np.where( inMask==1 )[0]
        if (len(okMask)>0):
            for kk in okMask:
                print kk
                rect = Rectangle((tx0[kk],ty0[kk]),xWidth[kk],yWidth[kk],color='b',ec='none',alpha=0.3)
                axDEBUG.add_patch(rect) 
#                axDEBUG.draw()

    # Run 1 Monte-Carlo realisation based on wt.
    # generate N random numbers [0,1]
    # if ith number < wt, include:
    wtMC = np.random.random(len(x0))
    
    for jj in range(len(x0)):
        ii=ss[jj]
#        ii=jj

        if (inMask[ii]==1):
            if(dbglvl>0): logging.debug('this object is already in the mask. skipping.')
            continue

        if( wtMC[ii] > wt[ii] ): continue
        
        # take one slit at a time from candidate list and compare with *all* existing slits currently in mask:        
        # http://tech-read.com/2009/02/06/program-to-check-rectangle-overlapping/
        r1x1=tx0[ii] ; r1x2=tx1[ii] ; r1y1=ty0[ii] ; r1y2=ty1[ii]

        okMask = np.where( inMask==1 )[0]
        if(dbglvl>0): logging.debug('%sth slit'%ii)
        if (len(okMask)<1):
            if(dbglvl>0): logging.debug('no objects currently in mask. accepting this new slit')
            inMask[ii]=1
        else:
            if (dbglvl>0): logging.debug('%s slits currently in mask'%(len(okMask)))

            r2x1=tx0[okMask] ; r2x2=tx1[okMask] ; r2y1=ty0[okMask] ; r2y2=ty1[okMask]
            olap = np.where( (r1x2 >= r2x1) & (r1y2 >= r2y1) & (r1x1 <= r2x2) & (r1y1 <= r2y2) )[0]
            if (len(olap)>0):
                if (dbglvl>0): 
                    logging.debug('%s slit collisions. Rejecting this new slit'%(len(olap)))
                    logging.debug('collides with: %s'%okMask[olap])
                    logging.debug('edges of current slit: %.2f %.2f'%(r1y1,r1y2))
                    logging.debug('edges of colliding slit(s): %.2f %.2f'%(r2y1[olap][0],r2y2[olap][0]))
                if(dbglvl>2): 
###                    rect = Rectangle((x0[ii],y0[ii]),xWidth[ii],yWidth[ii],color='r',ec='none',alpha=0.2)
                    rect = Rectangle((xCen[ii],ty0[ii]),10.0,yWidth[ii],color='r',ec='none',alpha=0.2)
                    axDEBUG.add_patch(rect) 
                continue
            else:
                inMask[ii]=1
                if (dbglvl>0): logging.debug('ADDING THIS SLIT!')
                if(dbglvl>1):
                    rect = Rectangle((x0[ii],ty0[ii]),xWidth[ii],yWidth[ii],color='y',ec='none',alpha=0.3)
                    axDEBUG.add_patch(rect) 
                    print 'here'
#                    axDEBUG.draw()
    
    okMask = np.where( inMask==1 )[0]
    if(dbglvl>0): 
        logging.debug('%s slits in mask'%(len(okMask)))
        print okMask
    if(dbglvl>1): plt.show()
    return inMask


def showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=0.0,ypad=0.0):
    tx0=x0-xpad/2.
    tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
    ty0=y0-ypad/2.
    ty1=y0+yWidth+ypad/2.

    figDEBUG = plt.figure()
    axDEBUG = figDEBUG.add_subplot(111)
    xCen = (tx0+tx1)/2.0 ; yCen = (ty0+ty1)/2.0 
    #plt.plot(xCen,yCen,'k,') # these points are spectral box centres)
    
    okMask = np.where( inMask==1 )[0]

    # plot objects scaled by weighting:
    ok=np.where(wt>0.0)[0]
    for kk in ok:
        sz = 100.*wt[kk]
        circ = Circle((xobj[kk],yobj[kk]),sz,color='r',alpha=0.3)
        #plt.text(xobj[kk],yobj[kk],'%.0f'%id[kk])
        plt.text(xobj[kk],yobj[kk],'%.0f'%kk)
        axDEBUG.add_patch(circ)
    bad = np.where(wt==0.0)[0]
    if (len(bad)>0):
        plt.plot(xobj[bad],yobj[bad],'rx')
        
    if (len(okMask)>0):
        for kk in okMask:
#            print kk
            # SPECTRUM:
            
            sz = 100.*wt[kk]
            circ = Circle((xobj[kk],yobj[kk]),sz,color='b',alpha=0.3)
            axDEBUG.add_patch(circ)
            
            rect = Rectangle((x0[kk],y0[kk]),xWidth[kk],yWidth[kk],color='g',ec='none',alpha=0.3)
            axDEBUG.add_patch(rect) 
            # slit:
            sx0=x0-swid/2.+xToLeft
            sx1=swid
            # in wavelength space
            slit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='m',ec='none')
            axDEBUG.add_patch(slit)
            # in direct image/mask
            sx0=xobj-swid/2.
            sx1=swid
            rslit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='none',ec='k')
            axDEBUG.add_patch(rslit)
            
            plt.axis('equal')
            plt.plot(xobj,yobj,'k,')
            plt.plot(xobj[okMask],yspe[okMask],'k,')
            plt.xlabel('GMOS x (Ham. binned pix)')
            plt.ylabel('GMOS y (Ham. binned pix)')
        #    if (title):
        #        plt.title(title)
            plt.title('%.0f slits'%np.sum(inMask))
            axDEBUG.set_xlim(-50,3000)
            axDEBUG.set_ylim(-50,2300)

    drawHam(axDEBUG)
    plt.show()

def mask2Reg(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=0.0,ypad=0.0):
    tx0=x0-xpad/2.
    tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
    ty0=y0-ypad/2.
    ty1=y0+yWidth+ypad/2.
    
    f=open('ds9.reg','w')
    f.write('# Region file format: DS9 version 4.1\n')
#    f.write('global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\n')
    f.write("physical\n")
    
    okMask = np.where( inMask==1 )[0]

    for kk in okMask:
        f.write('polygon(%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n'%(tx0[kk],ty0[kk],tx0[kk],ty1[kk],tx1[kk],ty1[kk],tx1[kk],ty0[kk],tx0[kk],ty0[kk]))
        f.write('circle(%.3f,%.3f,%.3f\n'%(xobj[kk],yobj[kk],5.0))
        
        # slit:
        f.write('polygon(%.3f,%.3f,%.3f,%.3f\n'%(xobj[kk],yobj[kk]-slen[kk]/2.0,xobj[kk],yobj[kk]+slen[kk]/2.0)) 

    f.close()
        
def setupStars(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=xpadPix,ypad=ypadPix,nMinStars=4):
        # currently just force ALL Setup stars found into mask before doing anything else
        # *** later optimise from list of candidates as part of mask optimisation
        
        # check if there are more than nMinStars, and if so, attempt to choose:
        candStars = np.where(wt==9.0)[0]
        nCandStars = len(candStars)
        if (nCandStars>nMinStars):
            #*** just take first 4 for now:
#            candStars = candStars[0:nMinStars]
            #***
            candStars = candStars[0]
            inMask[candStars]=1.0
        else:
            # force into mask.
            # we don't care aboot collisions between setup stars
            # BUT we should check later that these don't collide with must-haves
            inMask[candStars]=1.0
#        pass
    
def verifyMustHaves(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=xpadPix,ypad=ypadPix):
        # do some pre-checking of must-have objects, as optimisation code before passing
        # to runmask/resolveCollisions
        
#        okMask = np.where(wt>=1.0)[0]
        okMask = np.where(wt==1.0)[0] # don't include setup stars yet
        print 'okmask',okMask
        if (len(okMask)==0):
            # no must haves or acq objects
            return
        
        # run resolveCollisions on only musthave objects:
        tx0 = np.copy(x0[okMask]) ; ty0 = np.copy(y0[okMask])
        txWidth = np.copy(xWidth[okMask]) ;  tyWidth = np.copy(yWidth[okMask])
        twt = np.copy(wt[okMask]) ; tinMask = np.zeros(len(okMask)) ; tss=np.arange(len(okMask))
        txobj = np.copy(xobj[okMask]) ;   tyobj = np.copy(yobj[okMask])
        inMH = resolveCollisions(tx0,txWidth,ty0,tyWidth,twt,tinMask,tss,txobj,tyobj,xpad=xpadPix,ypad=ypadPix)
        if (inMH.min()<1.0):
            print '**** not all must-haves could be allocated! ****'
            stop()            
            # *** actually we should be a bit cleverer here:
            # collisions between setup stars are fine. 
            # collisions with any science slits is not
         
            # take each setup star in turn and check it does not collide with a must-have science slit:
        stars = np.where(wt==9.0)[0]
        oinMask=np.copy(inMask)

        """
        # kludge:
        if(len(stars)>4):
            #inMask[stars]=0.0 # remove all
            wt[stars]=0.0
            stars = stars[0:4]#*** just take first 4
            wt[stars]=9.0
        """
        
        if(len(stars)>0):
            print 'checking each setup star'
            for i in range(len(stars)):
                print 'star %s'%i
                inMask[stars[i]]=1.0
                tx0 = np.copy(x0[okMask]) ; ty0 = np.copy(y0[okMask])
                txWidth = np.copy(xWidth[okMask]) ;  tyWidth = np.copy(yWidth[okMask])
                twt = np.copy(wt[okMask]) ; tinMask = np.zeros(len(okMask)) ; tss=np.arange(len(okMask))
                txobj = np.copy(xobj[okMask]) ;   tyobj = np.copy(yobj[okMask])
                inMH = resolveCollisions(tx0,txWidth,ty0,tyWidth,twt,tinMask,tss,txobj,tyobj,xpad=xpadPix,ypad=ypadPix)
                if (inMH.min()<1.0):
                    print '**** star collided with must have science slit. REJECTED ****'
                    wt[stars[i]]=0.0
                inMask[stars[i]]=0.0 # take out again, and just try next star alone
            okMask=np.where(wt>=1.0)[0]
            inMask[okMask]=1.0
            
         
        
# See comments below in runMask()
def getShuffle(wt,sortScheme):
    if(sortScheme=='shuffle'):
        ss = np.argsort(np.random.random(len(wt))) # just randomise order
    elif(sortScheme=='wts'): # sort by priority
        ss=np.argsort(wt)[::-1]
    elif(sortScheme=='shufflewts'): # shuffle within bins of wts, ordered by wts from low to high:
        bins = np.arange(0.0,1.0,0.05)
        inbin = np.digitize(wt,bins=bins)
        ubins = np.unique(inbin)
        ss = -np.ones(len(wt))
        ilast=0
        for i in np.sort(ubins)[::-1]: # sort high to low
            if(dbglvl>1): print i
            ok = np.where(inbin==i)[0]
            sss = ok[np.argsort(np.random.random(len(ok)))]
            if(dbglvl>1): print ilast,ilast+len(ok), len(ok)
            ss[ilast:ilast+len(ok)] = sss
            ilast=ilast+len(ok)
            
        ss=ss.astype(int)
    # Finally, reject wt==0.0 points:
    # Done in runmask instead
#    bad=np.where(wt[ss]==0.0)[0]    
#    if(len(bad)>0):
#        ss=np.delete(ss,bad)
    return ss
 
def runMask(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=xpadPix,ypad=ypadPix,sortScheme='shuffle'):
    # run a few iterations of mask to ensure gaps are filled:

    
    # THIS BIT IS CRUCIAL
    # Set the order in which obejects are examined. As soon as a slit is assigned in a realisation it cannot 
    # be removed again. So, the order in which objects are tested is vital.
    #
    # I have previously done this as a function of:
    #  - distance from cluster centre
    #  - sorting by priority, ...
    #
    # Now I think optimal is probably randomising order, but sorting in bins of priority from low to high    
    ss = getShuffle(wt,sortScheme)
    # (need to reject wt==0.0 points below) 
    
    inMask1 = resolveCollisions(x0,xWidth,y0,yWidth,wt,inMask,ss,xobj,yobj,xpad=xpadPix,ypad=ypadPix)
    inMaski = np.copy(inMask1)
    del inMask1
    # Run 5 iterations with original weights, followed by 5 with essentially no weights (to fill in gaps).
    # May require more if a lot of the weights are v low (resulting in slits not being assigned).
    for kk in range(15):
#        wt09 = np.copy(wt)*0.0+0.9999
#        del ss
#        ss = getShuffle(wt,sortScheme)

        
        inMask1 = resolveCollisions(x0,xWidth,y0,yWidth,wt,inMaski,ss,xobj,yobj,xpad=xpadPix,ypad=ypadPix)
        inMaski = np.copy(inMask1)
        del inMask1
        okMask=np.where(inMaski==1.0)[0]
        ##print len(okMask)
    for kk in range(10):
        wt09 = np.copy(wt)*0.0+0.9999
        bad = np.where(wt==0.0) ; wt09[bad]=0.0
#        del ss
#        ss = getShuffle(wt,sortScheme)
        
        inMask1 = resolveCollisions(x0,xWidth,y0,yWidth,wt09,inMaski,ss,xobj,yobj,xpad=xpadPix,ypad=ypadPix)
        #inMask1 = resolveCollisions(x0,xWidth,y0,yWidth,wt,inMaski,ss,xobj,yobj,xpad=xpadPix,ypad=ypadPix)
        inMaski = np.copy(inMask1)
        del inMask1
        okMask=np.where(inMaski==1.0)[0]
        ##print len(okMask)
    
    
    
    okMask=np.where(inMaski==1.0)[0]
    print '%s slits in mask'%(len(okMask))
    return inMaski

#def getStars(starfile,id,xobj,yobj,ra,dec,wt,slen,swid,stilt):
#    id,xobj,yobj,ra,dec,wt,slen,swid,stilt = np.loadtxt(infile,unpack=True)

def desmask(cra,cdec,rotang,infile,niter=10,iplot=False,sortScheme='shuffle',starfile=None,NSmode='micro'):
 
    # NOTE: rotang is not actually needed for GoGREEN GMOS masks, since we're designing from pixel coords directly
 
    #(cra,cdec,rotang,infile) = (150.5702, 2.4986000000000002, 0.0, 'test1in.txt')
    #(cra,cdec,rotang,infile) = (150.5702, 2.4986000000000002, 0.0, 'COS221in1.txt')
    
    id,xobj,yobj,ra,dec,wt,slen,swid,stilt = np.loadtxt(infile,unpack=True)
    
    ## load setup stars and add to arrays:
#    if (starfile):
#        id,xobj,yobj,ra,dec,wt,slen,swid,stilt = getStars(starfile,id,xobj,yobj,ra,dec,wt,slen,swid,stilt)
    
    
    xspe,yspe,xToLeft,xToRight,yAbove,yBelow, keepFlag = defineGMOSspec(xobj,yobj,slen,wt,NSmode=NSmode)
    # set weight of keepflag==0 objects to 0.0
    rej=np.where(keepFlag==0.0)[0]
    if (len(rej)>0): wt[rej]=0.0
    
    x0 = xspe-xToLeft ; xWidth = xToLeft + xToRight
    y0 = yspe-yBelow ; yWidth = yAbove+yBelow
    
    
    
    inMask = np.zeros(len(id)) ; ok=np.where(wt>=1.0)[0] ; inMask[ok]=1.0
    print len(ok)
    
    # check for collisions between must-have objects
    verifyMustHaves(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj)
    
    for i in range(niter):
        inMask = np.zeros(len(id)) ; ok=np.where(wt>=1.0)[0] ; inMask[ok]=1.0
        #*** need to check for collisions between must have objects (and setup stars) and exit gracefully if problem ***
             
        inMask1 = runMask(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=xpadPix,ypad=ypadPix,sortScheme=sortScheme)
    
        if (i==0):
            storeRes = np.copy(inMask1)
        else:
            #print np.sum(inMask)
            storeRes = np.vstack((storeRes,inMask1))
        
        okMask = np.where(inMask1==1.0)[0]
        print 'ITER: %s'%i
        print '%s slits in Mask'%len(okMask)
    
    if niter>1:    
        tots = np.sum(storeRes,1)
        totWts = np.sum(storeRes*wt,1)
        
        bestN = np.argmax(tots)
        bestWt = np.argmax(totWts)
        
        print 'best N mask (i=%s): N=%s, Tot(wt)=%.2f'%(bestN,tots[bestN],totWts[bestN])
        print 'best Wt mask (i=%s): Tot(wt)=%.2f, N=%s'%(bestWt,totWts[bestWt],tots[bestWt])
        
        # restore best mask:
        del inMask
        inMask = np.ravel(storeRes[bestN])
    else: 
        inMask = np.copy(inMask1)
    
    
    outname = infile.replace('in','out')
    if (infile==outname):
        outname='maskout.txt'
    np.savetxt(outname,np.transpose((ra[okMask],dec[okMask],wt[okMask],id[okMask])),fmt='%.7f %.7f %.4f %s')
    gmmpsname = infile.replace('in','gmmps')
    mag = np.repeat(20,len(okMask))
    pri = np.ones(len(okMask))
    pstars = np.where(wt[okMask]>=9.0)[0]
    if (len(pstars)>0):
        pri[pstars]=0.0 # is this right?
    if (NSmode=='micro'):
        slenfac=2.0
    else:
        slenfac=1.0
    # pixels in unbinned coords. nod offset***?
    np.savetxt(gmmpsname,np.transpose((id[okMask],ra[okMask]/15.0,dec[okMask],\
                                     xobj[okMask]*2.0,yobj[okMask]*2.0,mag,pri,slen[okMask]*pxscale/slenfac,\
                                     swid[okMask]*pxscale,stilt[okMask])),\
                                     fmt='%.0f %.8f %.7f %.4f %.4f %.3f %s %.3f %.3f %.3f')

    
    #        gmi.ID = gal.id ; gmi.RA = gal.ra/15.0 ; gmi.DEC = gal.dec ; gmi.x_ccd = gal.xp ; gmi.y_ccd = gal.yp
        #gmi.MAG = np.repeat(21.0,len(gal)) ; gmi.priority = np.repeat(1.,len(ok))

    
    # Plot final mask:
    if(iplot):
        showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=0.0,ypad=0.0)
        showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=xpadPix,ypad=ypadPix)
        
        
    mask2Reg(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=0.0,ypad=0.0)

        
        
    
    if(dbglvl>1):
        rejMask = np.where(inMask==0.0)[0]
        for i in rejMask:
            plt.plot([xobj[i]-swid[i]/2.0,xobj[i]-swid[i]/2.0], [yobj[i]-yBelow[i],yobj[i]+yAbove[i]], 'r-')
            plt.plot([xobj[i]+swid[i]/2.0,xobj[i]+swid[i]/2.0], [yobj[i]-yBelow[i],yobj[i]+yAbove[i]], 'r-')
            plt.plot([xobj[i]-swid[i]/2.0,xobj[i]+swid[i]/2.0], [yobj[i]-yBelow[i],yobj[i]-yBelow[i]], 'r-')
            plt.plot([xobj[i]-swid[i]/2.0,xobj[i]+swid[i]/2.0], [yobj[i]+yAbove[i],yobj[i]+yAbove[i]], 'r-')
            plt.text(xobj[i],yobj[i],i,color='r')
        plt.show()

