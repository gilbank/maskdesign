
import os,sys,glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from astropy.io import fits
from astropy.io.fits import Column
#from astropy.cosmology import arcsec_per_kpc_proper as asperkpc
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

from numpy.lib import recfunctions as recf

from config import *



#nMasks=5
#name='SpARCS_0035'
#name='SPT-0205'
#name='CDFS-41'


#nMasks=3
#name='COS-221'
#name='SXDF64'
#name='SXDF76'
#name='SXDF87'



def match(a,b,badval=-1):
    # much faster matching using dictionary!
    good=np.where(b <> badval)[0]
    d=dict(zip(b[good], np.arange(len(b))[good]  ))
    okd = [d.get(x) for x in a]
    # result will be none if there is no match. need to deal
    okddd=np.array(okd)
    ok=np.array(okddd.nonzero()).astype('int64')
    
    okb=(okddd[ok]).astype('int64')
    oka=ok
        
#    return np.array(oka),np.array(okb)
    return np.reshape(np.array(oka),-1),np.reshape(np.array(okb),-1)



def inBin(x,bin):
    ok=np.where( (x>bin[0]) & (x<=bin[1]) )[0]
    return ok

def getZphWts(gal,fInfo):
        phzWt = np.zeros(len(gal))
        # -- 2nd highest prioirty photoz (within: 0.7--2.0):
        ok = np.where( (gal.zph>0.7) & (gal.zph<2.0) )[0]
        #sel2 = np.where( (dat.zPDF>0.7) & (dat.zPDF<2.0) )[0]
        phzWt[ok] = 0.5
        
        # -- highest priority photo-z (within 2sigma of grpz):
        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph >= fInfo['grpz']) )[0]
#        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph>=grpz), 1.0, 0.0 )[0]
        #   sel = np.where( (dat.zPDF>0) & (2.0*dat.zPDF_l68-dat.zPDF <= grpz) & (2.*dat.zPDF_u68-dat.zPDF>=grpz) & (dat.TYPE==0) )[0]
        phzWt[ok] =1.0
        # *** NOTE: gals with phzs outside this range (or bad photozs) are hence completely rejected! ***
        return phzWt


def updateinMask(gal,maskfile):
    mra,mdec,mpri,mID = np.loadtxt(maskfile, unpack=True)
    ok1,ok2=match(gal.ID,mID)
    gal.inMask[ok1]=1.0

def calcdisMpc(fInfo,gal):
    # add distance of every gal from rabcg, decbcg in Mpc
    
    
    dra = (gal.ra - fInfo['grpra'])*np.cos(np.deg2rad(gal.dec))
    ddec = gal.dec - fInfo['grpdec']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['grpz'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
#    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
#    del gal
#    gal = np.copy(tgal)
#    del tgal
    adisMpc = disMpc
    
    if(0): # use spm instead
        from astrometry.libkd import spherematch as spm
        oka,okb,disdeg = spm.match_radec(gal.ra,gal.dec,fInfo['grpra'],fInfo['grpdec'],1.0,nearest=False)
        adisMpc = np.ones(len(gal))
        disMpc = disdeg*3600.0 / convfac /1000.0
        adisMpc[oka] = disMpc # needed with spm
    

    return adisMpc


def evalCat(gal,fInfo):

    redSeqCoeffs = np.array((fInfo['RSslope'],fInfo['RSintcpt']))
    redCoeffs = redSeqCoeffs+np.array((0,dRed))
    redLimCoeffs = redSeqCoeffs+np.array((0,dRedLim))

    
    col = gal.zmag - gal.irac1
    
    # Find number of objects in appropriate mag,distance, etc. bins in whole catalogue (with appropriate cuts)
    # for completeness calculations:
    inner = (gal.disMpc<=0.5)
    outer = ( (gal.disMpc>0.5) & (gal.disMpc<=1.0) )
    outsk = ( (gal.disMpc>1.0) )
    
    bri = ( (gal.zmag > zBri[0]) & (gal.zmag <= zBri[1]) )
    fnt = ( (gal.zmag > zFnt[0]) & (gal.zmag <= zFnt[1]) )
    
    # -- redder than CMR:
    #yfitRedLim = np.polyval(redLimCoeffs,gal.irac1)
    yfitRedLim = np.polyval(redCoeffs,gal.irac1)

    # -- blue exclusion cut:
    # (careful, this is in terms of irac1 mag, not z, as above)
    yfitBlueLim = np.polyval(blueCoeffs,gal.irac1)
        

    # othCuts must be applied to each condition!
    if (gal.izph[0]):
        print 'using photozs'
        zphWts = getZphWts(gal,fInfo)
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal.irac1<irac1VFnt) \
                    & (gal.irac1>0.0) \
#                    & (zphWts>0.6) )
                    & (zphWts>0.4) )

    else:
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal.irac1<irac1VFnt) \
                    & (gal.irac1>0.0) \
                    )


    hasSlit = (gal.inMask>0)

    innBri = np.sum( inner & bri & othCuts )
    outerBri = np.sum( outer & bri & othCuts )
    outskBri = np.sum( outsk & bri & othCuts )
    innFnt = np.sum( inner & fnt & othCuts )
    outerFnt = np.sum( outer & fnt & othCuts )
    outskFnt = np.sum( outsk & fnt & othCuts )
#    print 'len(innBri)=%s'%len(innBri)
    #print innBri,outerBri,innFnt,outerFnt,outskFnt
    innBriS = np.sum( inner & bri & othCuts & hasSlit)
    outerBriS = np.sum( outer & bri & othCuts & hasSlit)
    outskBriS = np.sum( outsk & bri & othCuts & hasSlit)
    innFntS = np.sum( inner & fnt & othCuts & hasSlit)
    outerFntS = np.sum( outer & fnt & othCuts & hasSlit)
    outskFntS = np.sum( outsk & fnt & othCuts & hasSlit)
    #print innBriS,outerBriS,innFntS,outerFntS,outskFntS

    print
    print 'Bright:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innBriS,innBri, outerBriS,outerBri, outskBriS,outskBri )
    print 'Faint:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innFntS,innFnt, outerFntS,outerFnt, outskFntS,outskFnt )

    return np.array((innBriS, outerBriS, outskBriS)),\
            np.array((innBri, outerBri, outskBri)),\
            np.array((innFntS, outerFntS, outskFntS)),\
            np.array((innFnt, outerFnt, outskFnt)),\


def doName(name):#,strategy):

    rootdir='/home/gilbank/Proj/goGreen/MaskDesign/'

    fInfo = fits.getdata('%s%sinfo.fits'%(rootdir,name)) ; fInfo=fInfo[0] # convert to scalars
    gal = fits.getdata('%s%sgals.fits'%(rootdir,name))

    redSeqCoeffs = np.array((fInfo['RSslope'],fInfo['RSintcpt']))
    redCoeffs = redSeqCoeffs+np.array((0,dRed))
    redLimCoeffs = redSeqCoeffs+np.array((0,dRedLim))

    # CAREFUL: this must only be reset at the start of 
    inMask = np.zeros(len(gal)) # flag whether gal has been assigned to a mask or not:
    prob = np.zeros(len(gal)) # probability of being included in mask
    
    
    # calculate distances:
    disMpc = calcdisMpc(fInfo,gal)
    
    # add all these other arrays to gal array, to make sure everything stays in sync:
    gal = recf.append_fields(gal,('inMask','prob','disMpc'),(inMask,prob,disMpc),asrecarray=True,usemask=False)
    # trim to field:
    inFoV = np.where( (gal.xp>xminField) & (gal.xp<=xmaxField) &  (gal.yp>yminField) & (gal.yp<=ymaxField))[0]
    gal = gal[inFoV]
    
    print '%s gals in full catalogue within imaging field'%(len(gal))
    print

#    print '%sTestStrat/%s/best%sin*txt'%(rootdir,strategy,name)
    tfiles = glob.glob('best%sin*txt'%(name))
    files = np.sort(np.array(tfiles))
    
    nMasks = len(files)
    
    anBriS=np.zeros((3,nMasks)) ; anBri=np.zeros((3,nMasks))
    anFntS=np.zeros((3,nMasks)) ; anFnt=np.zeros((3,nMasks))
    #,nBri, nFntS, nFnt
    
    for i in range(nMasks):
        inname = files[i]
        print inname
        # evaluate design:
        maskname = inname.replace('in','out')
        maskname = maskname.replace('Faoutt','Faint') # :)
        mra,mdec,mpri,mID = np.loadtxt(maskname, unpack=True)
        ok1 = np.where(gal.inMask==1)[0]
        
        updateinMask(gal,maskname)
        
        print '%s: %s slits; total weight=%.2f'%(maskname,len(mra),np.sum(mpri))
        print
        
        
        (nBriS,nBri, nFntS, nFnt) = evalCat(gal,fInfo)       
        anBriS[:,i] = nBriS ; anBri[:,i] = nBri
        anFntS[:,i] = nFntS ; anFnt[:,i] = nFnt

    return anBriS,anBri,anFntS,anFnt


def doOne(name,quant='N'):
    
    plt.figure()
    if(1):
        anBriS,anBri,anFntS,anFnt = doName(name)
        if(quant=='N'):
            
            val1 = np.sum(anFntS[0:1,:],0) ; plt.ylabel('N in mask') ; plt.ylim([0,75])
            valB1 = np.sum(anBriS[0:1,:],0)
            val2 = np.sum(anFntS[0:2,:],0) 
            valB2 = np.sum(anBriS[0:2,:],0)
            val3 = np.sum(anFntS[0:3,:],0) 
            valB3 = np.sum(anBriS[0:3,:],0)

        
        
        else:
            val1 = np.sum(anFntS[0:1,:],0)/np.sum(anFnt[0:1,:],0) ; plt.ylabel('completeness') ; plt.ylim([0,1])
            valB1 = np.sum(anBriS[0:1,:],0)/np.sum(anBri[0:1,:],0) 
            val2 = np.sum(anFntS[0:2,:],0)/np.sum(anFnt[0:2,:],0)
            valB2 = np.sum(anBriS[0:2,:],0)/np.sum(anBri[0:2,:],0) 
            val3 = np.sum(anFntS[0:3,:],0)/np.sum(anFnt[0:3,:],0)
            valB3 = np.sum(anBriS[0:3,:],0)/np.sum(anBri[0:3,:],0) 

        
        
        Nmasks = anBri.shape[1]
        plt.plot(np.arange(Nmasks)+1,val1,'o-',color='m',label='Fnt (r<0.5Mpc)')
        plt.plot(np.arange(Nmasks)+1,valB1,'o--',color='m')
        plt.plot(np.arange(Nmasks)+1,val2,'o-',color='g',label='Fnt (r<1.0Mpc)')
        plt.plot(np.arange(Nmasks)+1,valB2,'o--',color='g')
        plt.plot(np.arange(Nmasks)+1,val3,'o-',color='k',label='Fnt (all r)')
        plt.plot(np.arange(Nmasks)+1,valB3,'o--',color='k',label='Bri')


        
    plt.legend(numpoints=1,loc='upper left')
    plt.xlabel('Nmasks')
    
    
    plt.title(name)
    plt.show()
    plt.savefig('%s_%s.png'%(name,quant))


# r<1.0 as fn of mask #
def doClus(strategy='Default', quant='N'):
    clusnames = ['SpARCS_0035','SPT-0205','CDFS-41']
    #groupnames = ['COS-221','SXDF64','SXDF76','SXDF87']
    clusnames = ['SpARCS_0035','SPT-0205','CDFS-41','COS-221','SXDF64','SXDF76','SXDF87']

    cols = ['b','g','r','m','c','y','k']
    plt.figure()
    for i,name in enumerate(clusnames):
        anBriS,anBri,anFntS,anFnt = doName(name,strategy)
        if(quant=='N'):
            val = np.sum(anFntS[0:2,:],0) ; plt.ylabel('N in mask (r<1.0Mpc)') # inner two radial bins of fnt:
            valB = np.sum(anBriS[0:2,:],0) ; plt.ylabel('N in mask (r<1.0Mpc)') # inner two radial bins of fnt:
        else:
            val = np.sum(anFntS[0:2,:],0)/np.sum(anFnt[0:2,:],0) ; plt.ylabel('completeness (r<1.0Mpc)')
            valB = np.sum(anBriS[0:2,:],0)/np.sum(anBri[0:2,:],0) ; plt.ylabel('completeness (r<1.0Mpc)') # inner two radial bins of fnt:
        Nmasks = anBri.shape[1]
        plt.plot(np.arange(Nmasks)+1,val+0.1*float(i),'o-',color=cols[i],label='%s Fnt'%name)
#        plt.plot(np.arange(Nmasks)+1,valB+0.1*float(i),'o--',color=cols[i],label='%s Bri'%name)
        plt.plot(np.arange(Nmasks)+1,valB+0.1*float(i),'o--',color=cols[i])
        
#    plt.legend(numpoints=1,loc='upper left')
    plt.xlabel('Nmasks')
    
    plt.title(strategy)
    plt.show()
    
    
# r < 0.5 as fn of mask #
def doClusr05(strategy='Default', quant='N'):
    clusnames = ['SpARCS_0035','SPT-0205','CDFS-41']
    #groupnames = ['COS-221','SXDF64','SXDF76','SXDF87']
    clusnames = ['SpARCS_0035','SPT-0205','CDFS-41','COS-221','SXDF64','SXDF76','SXDF87']

    cols = ['b','g','r','m','c','y','k']
    plt.figure()
    for i,name in enumerate(clusnames):
        anBriS,anBri,anFntS,anFnt = doName(name,strategy)
        if(quant=='N'):
            val = np.sum(anFntS[0:1,:],0) ; plt.ylabel('N in mask (r<0.5Mpc)') # inner two radial bins of fnt:
            valB = np.sum(anBriS[0:1,:],0) ; plt.ylabel('N in mask (r<0.5Mpc)') # inner two radial bins of fnt:
        else:
            val = np.sum(anFntS[0:1,:],0)/np.sum(anFnt[0:1,:],0) ; plt.ylabel('completeness (r<0.5Mpc)')
            valB = np.sum(anBriS[0:1,:],0)/np.sum(anBri[0:1,:],0) ; plt.ylabel('completeness (r<0.5Mpc)') # inner two radial bins of fnt:
        Nmasks = anBri.shape[1]
        plt.plot(np.arange(Nmasks)+1,val+0.1*float(i),'o-',color=cols[i],label='%s Fnt'%name)
#        plt.plot(np.arange(Nmasks)+1,valB+0.1*float(i),'o--',color=cols[i],label='%s Bri'%name)
        plt.plot(np.arange(Nmasks)+1,valB+0.1*float(i),'o--',color=cols[i])
        
#    plt.legend(numpoints=1,loc='upper left')
    plt.xlabel('Nmasks')
    
    plt.title(strategy)
    plt.show()

# as fn of r for all available masks
def doClusfr(strategy='Default', quant='N'):
    clusnames = ['SpARCS_0035','SPT-0205','CDFS-41']
    #groupnames = ['COS-221','SXDF64','SXDF76','SXDF87']
    clusnames = ['SpARCS_0035','SPT-0205','CDFS-41','COS-221','SXDF64','SXDF76','SXDF87']

    cols = ['b','g','r','m','c','y','k']
    plt.figure()
    for i,name in enumerate(clusnames):
        anBriS,anBri,anFntS,anFnt = doName(name,strategy)

        
        if(quant=='N'):
            try:
                val = anFntS[:,-1] ; plt.ylabel('N in mask at R') # inner two radial bins of fnt:
                valB = anBriS[:,-1] ; plt.ylabel('N in mask at R') # inner two radial bins of fnt:
            except:
                pass

        else:
            val = anFntS[:,-1]/anFnt[:,-1] ; plt.ylabel('completeness at R')
            valB = anBriS[:,-1]/anBri[:,-1] ; plt.ylabel('completeness at R') # inner two radial bins of fnt:
        Nmasks = anBri.shape[1]
        rads=np.array([0.5,1.0,2.0])
        plt.plot(rads,val+0.1*float(i),'o-',color=cols[i],label='%s Fnt'%name)
        #        plt.plot(np.arange(Nmasks)+1,valB+0.1*float(i),'o--',color=cols[i],label='%s Bri'%name)
        plt.plot(rads,valB+0.1*float(i),'o--',color=cols[i])
        
#    plt.legend(numpoints=1,loc='upper left')
    plt.xlabel('R (Mpc)')
    
    plt.title(strategy)
    plt.show()

   
    

def doAll():
    strat = ['Default','StartFaint','BandEnd','BandStart']
    for item in strat:
        plt.close('all')
        doClusfr(strategy=item,quant='N')
        plt.savefig('%s_N_radius.png'%item)

    for item in strat:
        plt.close('all')
        doClusfr(strategy=item,quant='c')
        plt.savefig('%s_cpltness_radius.png'%item)





