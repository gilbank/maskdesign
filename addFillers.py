#
# Read in photometric catalogue and set up weighting scheme ready for maskdes
# Make checking plots (spatial, CMD,..)
#
import os,sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from astropy.io import fits
from astropy.io.fits import Column
#from astropy.cosmology import arcsec_per_kpc_proper as asperkpc
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

from numpy.lib import recfunctions as recf
from glob import glob

from config import *
#from configE2v import *

##from astrometry.libkd import spherematch as spm

import maskdesHam as MD2


#import maskdesE2v as MD2
# try to add fillers into existing masks



isilent=1
nMasks=5
#name='SpARCS_0035'
#name='SPT-0205'
#name='CDFS-41'
name='SPT-0546'

#nMasks=3
#name='COS-221'
#name='SXDF64'
#name='SXDF76'
#name='SXDF87'


# //////////////////////////////////////////////////////////////////////
# ---- Parameters for mask design setup
#nMasks=5 # number of masks in set for this cluster
# weighting scheme for each mask in sequence. Probably want different scheme for bright mask at start, followed by same for all subsequent
#allocateSchemes=np.repeat('bright2         ',nMasks)
#allocateSchemes[0]='faint'
#allocateSchemes[0]='bright   '
#allocateSchemes[0]='brightcore'

#allocateSchemes = ['first', 'fillfaint', 'rptfnt', 'rptfnt', 'rptfnt']


xc,yc = ccd_cx,ccd_cy

# //////////////////////////////////////////////////////////////////////


import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage


from match import simpmatch as match

def inBin(x,bin):
    ok=np.where( (x>bin[0]) & (x<=bin[1]) )[0]
    return ok

def getZphWts(gal,fInfo):
        phzWt = np.zeros(len(gal)) + 0.05
        # -- 2nd highest prioirty photoz (within: 0.7--2.0):
        ok = np.where( (gal.zph>0.7) & (gal.zph<2.0) )[0]
        #sel2 = np.where( (dat.zPDF>0.7) & (dat.zPDF<2.0) )[0]
        phzWt[ok] = 0.5
        
        # -- highest priority photo-z (within 2sigma of grpz):
        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph >= fInfo['grpz']) )[0]
#        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph>=grpz), 1.0, 0.0 )[0]
        #   sel = np.where( (dat.zPDF>0) & (2.0*dat.zPDF_l68-dat.zPDF <= grpz) & (2.*dat.zPDF_u68-dat.zPDF>=grpz) & (dat.TYPE==0) )[0]
        phzWt[ok] =1.0
        # *** NOTE: gals with phzs outside this range (or bad photozs) are hence completely rejected! ***
        return phzWt


def genWeights(gal,fInfo,scheme='filler'):
    col = gal.zmag - gal.irac1 ; mag = gal.irac1 ; zmag = gal.zmag
    pri = gal.prob

    print scheme

    # -- redder than CMR:

    # increase red limit
    yfit = np.polyval(redCoeffs,mag)
    yfitLim = np.polyval(redLimCoeffs,mag)

#    rej = np.where(col > yfit)[0]


    if (scheme=='filler'):
        # set bright high priority
        fntWt = 0.00
        briWt = 0.95
        ok = inBin(zmag,zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,zBri) ; pri[ok]=briWt
        rej = np.where( (col > yfitLim) | (zmag<zVBri[0]) | (zmag>zBri[1]) )[0]
##        pri[rej] = 0.0###1#5 
#        ok = inBin(zmag,zVBri) ; pri[ok]=0.05
#        ok = np.where( (gal.inMask>0) & (gal.zmag<zBri[1]) )[0]
#        print '%s bright gals in previous mask being ignored'%(len(ok))
#        pri[ok]=0.0

        ok=np.where(zmag<zBri[1])[0]
        pri[ok]=0.95
        
    else:
        print 'scheme %s not found'%scheme
        print flibble # to crash
        
    
    
###    ok = inBin(zmag,zVFnt) ; pri[ok]=0.#1
###    ok = inBin(zmag,zVBri) ; pri[ok]=0.01#5
    
    
    # -- redder than CMR:
    yfit = np.polyval(redLimCoeffs,mag)
#    rej = np.where(col > yfit)[0]
    rej = np.where( (col > yfit) & (zmag>zVBri[0]) & (zmag<zVFnt[1]) )[0]
#    pri[rej] = 0.0###1#5 

    yfit = np.polyval(redLimCoeffs,mag)
#    rej = np.where(col > yfit)[0]
    rej = np.where( (col > yfit) & (zmag>zVBri[0]) & (zmag<zVFnt[1]) )[0]
##    pri[rej] = 0.00 
    
    # --
    
    
    # -- blue exclusion cut:
    # (careful, this is in terms of irac1 mag, not z, as above)
    yfit = np.polyval(blueCoeffs,mag)
    rej = np.where(col < yfit)[0]
##    pri[rej]=0.0
    # --
    
    # -- irac1 exclusion cut:
    rej = np.where(mag>irac1VFnt)[0]
##    pri[rej]=0.0
    bad = np.where(mag>irac1Lim)[0]
##    pri[bad]=pri[bad]*0.#1 # downweight v faintest bin

    
    bad = np.where(mag<0.0)[0]
##    pri[bad]=0.0 # IRAC undetected?***
    # --

    
    radwt =  (1. - 0.4*gal.disMpc)**2 #*** probably not extreme enough
    
    
    # update weights:
    gal.prob = pri
    
    if (iRadialWt):
        notMustHaves = np.where(pri<1.0)[0]
        gal.prob[notMustHaves] = pri[notMustHaves]* radwt[notMustHaves]






def plotCMD(gal,fInfo,axname):
    col = gal.zmag - gal.irac1 ; mag = gal.irac1 
    pri = gal.prob
    axname.plot(mag,col,'k,')
    axname.set_xlabel('[3.6]')
    axname.set_ylabel('z - [3.6]')
    axname.set_xlim([19,23])
    axname.set_ylim([-2,6])
    
    xxx = np.arange(0,30)
    axname.plot(xxx,zFnt[1]-xxx,'b--',label='faint')
    axname.plot(xxx,zBri[1]-xxx,'b:',label='faint')
    axname.plot(xxx,np.polyval(blueCoeffs,xxx),'b-')
    

    axname.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r:')
    axname.plot(xxx,np.polyval(redCoeffs,xxx),'r-')
    axname.plot(xxx,np.polyval(redLimCoeffs,xxx),'r--')
    
    # wts:
    sz=pri*100 +0.1
    axname.scatter(mag,col,s=sz,alpha=0.5,color='b')
    

# initial plot to identify location of cluster in ra,dec and CMD    
def plotClus(gal,fInfo):
    col = gal.zmag - gal.irac1 ; mag = gal.irac1 
    pri = gal.prob
    xp = gal.xp ; yp = gal.yp
    
    plt.figure()
    plt.subplot(121)
    plt.plot(mag,col,'k,')
    if (gal.izph[0]):
        phzWt = getZphWts(gal,fInfo)
#        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05)  & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim))[0]
###        plt.plot( mag[ok],col[ok], 'or', alpha=1, label='|zph-grpz|<=0.05')
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3, label=r'|zph-grpz|<=2$\sigma$')
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=1.0) )[0]
        plt.plot( mag[ok],col[ok], 'or',alpha=0.1,label='zph= 0.7--2.0')
    else:
        ok=np.where( (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3)
        ok=np.where( (gal.disMpc<=0.3) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'ok', alpha=0.5, label='r<0.3Mpc')
        ok=np.where( (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.1)
        
    plt.xlabel('[3.6]')
    plt.ylabel('z - [3.6]')
    plt.xlim([19,23])
    plt.ylim([-2,6])
    plt.legend(numpoints=1)
    
    xxx = np.arange(0,30)
    plt.plot(xxx,zFnt[1]-xxx,'b--',label='faint')
    plt.plot(xxx,zBri[1]-xxx,'b:',label='bright')
    plt.plot(xxx,np.polyval(blueCoeffs,xxx),'b-')
    
    plt.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r:',label='RS')
    plt.plot(xxx,np.polyval(redCoeffs,xxx),'r--',label='RS+%.2f'%dRed)
    plt.plot(xxx,np.polyval(redLimCoeffs,xxx),'r-',label='RS+%.2f'%dRedLim)
    plt.legend(numpoints=1)
    
    # spatial:
    plt.subplot(122)
    plt.plot(gal.ra,gal.dec,'k,')

    plt.plot(fInfo['grpra'],fInfo['grpdec'],'og',ms=20,alpha=0.3)
    if (gal.izph[0]):
        phzWt = getZphWts(gal,fInfo)
#        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
###        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=1)
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=5.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.2)
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=5.5) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or',alpha=0.1)
    else:  
        ok=np.where( (gal.disMpc<=5.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.2)
        
 
    plt.xlabel('ra')
    plt.ylabel('dec')
    
    
    plt.show()
    
    
# in pixels
def drawMpcCircle(xc,yc,nMpc,fInfo,axname):
    radius = (nMpc*1000.0*asperkpc(fInfo['grpz'])).value # arcsec
    radius = radius/pxscale # pix
    circ = Circle((xc,yc),radius, fill=False, facecolor='none', edgecolor='r')
    axname.add_patch(circ)

def calcdisMpc(fInfo,gal):
    # add distance of every gal from rabcg, decbcg in Mpc
    
    
    dra = (gal.ra - fInfo['grpra'])*np.cos(np.deg2rad(gal.dec))
    ddec = gal.dec - fInfo['grpdec']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['grpz'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
#    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
#    del gal
#    gal = np.copy(tgal)
#    del tgal
    adisMpc = disMpc
    
    if(0): # use spm instead
        from astrometry.libkd import spherematch as spm
        oka,okb,disdeg = spm.match_radec(gal.ra,gal.dec,fInfo['grpra'],fInfo['grpdec'],1.0,nearest=False)
        adisMpc = np.ones(len(gal))
        disMpc = disdeg*3600.0 / convfac /1000.0
        adisMpc[oka] = disMpc # needed with spm
    

    return adisMpc

def plotSpatial(gal,fInfo,axname):
    xp = gal.xp ; yp = gal.yp 
    pri = gal.prob
    axname.plot(xp,yp,'k,')
    axname.set_xlabel('xp')
    axname.set_ylabel('yp')
#    axname.set_xlim([19,23])
#    axname.set_ylim([-2,6])

    # wts:
    sz=pri*100 +0.1
    axname.scatter(xp,yp,s=sz,alpha=0.5,color='b')

#    MD.drawGMOS()
    MD2.drawHam(axname)
    
    # draw 0.5 Mpc radius circle
    ##xc,yc = MD.MaskpixfromSky(fInfo['cra'],fInfo['cdec'],fInfo['rotang'],fInfo['grpra'],fInfo['grpdec'])
    drawMpcCircle(xc,yc,0.5,fInfo,axname)
    drawMpcCircle(xc,yc,1.0,fInfo,axname)
    drawMpcCircle(xc,yc,2.0,fInfo,axname)

def writeInput(inname,gal,fInfo,sLen=nomsLen,sWid=nomsWid,sTilt=nomsTilt,stars=None):
    # write the input file for desmask()
    #ID,xobj,yobj,ra,dec,pri,slen,swid,stilt = np.loadtxt('tmpCDFS41.dat',unpack=True)


    use=np.where(gal.prob>0.0)[0]
#    np.savetxt(inname,np.transpose( (gal.ID,gal.xp,gal.yp,gal.ra,gal.dec,gal.prob,\
#                                             np.repeat(sLen,len(gal)), np.repeat(sWid,len(gal)), np.repeat(sTilt,len(gal)) ) ) )

    f = open(inname,'w')
    np.savetxt(f,np.transpose( (gal.ID[use],gal.xp[use],gal.yp[use],gal.ra[use],gal.dec[use],gal.prob[use],\
                                             np.repeat(sLen,len(use)), np.repeat(sWid,len(use)), np.repeat(sTilt,len(use)) ) ),\
                                             fmt='%s %.2f %.2f %.7f %.7f %.4f %.2f %.2f %.3f' )
    
    try: #(len(stars))
        use=np.arange(len(stars)) ; stars.prob = np.repeat(9.0,len(stars))
        np.savetxt(f,np.transpose( (stars.ID[use],stars.xp[use],stars.yp[use],stars.ra[use],stars.dec[use],stars.prob[use],\
                                              np.repeat(starsLen,len(use)), np.repeat(starsWid,len(use)), np.repeat(0.0,len(use)) ) ),\
                                              fmt='%s %.2f %.2f %.7f %.7f %.4f %.2f %.2f %.3f' )
    except:
        pass
    f.close()
    
    
def tagMask(gal,fInfo,mID,masknum=1):    
    # add inMask bitflag:
    ok1,ok2 = match(gal.ID,mID)
#    mag = zmag[ok1]
    mgal = gal[ok1]
    mag = mgal.zmag
    # update inMask flag with binary to indicate which number(s) mask in
    gal.inMask[ok1] = gal.inMask[ok1] + 2**(masknum-1)


def evalCat(gal,fInfo):
    col = gal.zmag - gal.irac1
    
    # Find number of objects in appropriate mag,distance, etc. bins in whole catalogue (with appropriate cuts)
    # for completeness calculations:
    inner = (gal.disMpc<=0.5)
    outer = ( (gal.disMpc>0.5) & (gal.disMpc<=1.0) )
    outsk = ( (gal.disMpc>1.0) )
    
    bri = ( (gal.zmag > zBri[0]) & (gal.zmag <= zBri[1]) )
    fnt = ( (gal.zmag > zFnt[0]) & (gal.zmag <= zFnt[1]) )
    
    # -- redder than CMR:
    yfitRedLim = np.polyval(redLimCoeffs,gal.irac1)

    # -- blue exclusion cut:
    # (careful, this is in terms of irac1 mag, not z, as above)
    yfitBlueLim = np.polyval(blueCoeffs,gal.irac1)

    if (gal.izph[0]):
        zphWts = getZphWts(gal,fInfo)

    # othCuts must be applied to each condition!
    if (gal.izph[0]):
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal.irac1<irac1VFnt) \
                    & (gal.irac1>0.0) \
                    & (zphWts>0.4) )
    else:
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal.irac1<irac1VFnt) \
                    & (gal.irac1>0.0) \
                    )


    hasSlit = (gal.inMask>0)

    innBri = np.sum( inner & bri & othCuts )
    outerBri = np.sum( outer & bri & othCuts )
    outskBri = np.sum( outsk & bri & othCuts )
    innFnt = np.sum( inner & fnt & othCuts )
    outerFnt = np.sum( outer & fnt & othCuts )
    outskFnt = np.sum( outsk & fnt & othCuts )
#    print 'len(innBri)=%s'%len(innBri)
    #print innBri,outerBri,innFnt,outerFnt,outskFnt
    innBriS = np.sum( inner & bri & othCuts & hasSlit)
    outerBriS = np.sum( outer & bri & othCuts & hasSlit)
    outskBriS = np.sum( outsk & bri & othCuts & hasSlit)
    innFntS = np.sum( inner & fnt & othCuts & hasSlit)
    outerFntS = np.sum( outer & fnt & othCuts & hasSlit)
    outskFntS = np.sum( outsk & fnt & othCuts & hasSlit)
    #print innBriS,outerBriS,innFntS,outerFntS,outskFntS

    print
    print 'Bright:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innBriS,innBri, outerBriS,outerBri, outskBriS,outskBri )
    print 'Faint:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innFntS,innFnt, outerFntS,outerFnt, outskFntS,outskFnt )

    return np.array((innBriS, outerBriS, outskBriS)),\
            np.array((innBri, outerBri, outskBri)),\
            np.array((innFntS, outerFntS, outskFntS)),\
            np.array((innFnt, outerFnt, outskFnt)),\


def evalMask(gal,fInfo,masknum=0,nTotMasks=3):
#    # evaluate mask:

    # if masknum==0, evaluate all objects inMask, otherwise only specified mask number:
    if (masknum):
#        reqBit = 2**(masknum-1)
        bitFlag = [np.binary_repr(y,nTotMasks) for y in gal.inMask.astype('int')]        
#        ok1 = np.where(gal.inMask==reqBit)[0]
        ok1 = [j for j,y in enumerate(bitFlag) if y[nTotMasks-masknum]=='1' ]
        #ok1 = [j for j,y in enumerate(maskFlag) if y[2]=='1']
    else:
        ok1 = np.where(gal.inMask>0)[0]
    
#    mag = zmag[ok1]
    mgal = gal[ok1]
    mag = mgal.zmag
    # as fn of zmag:
#    print '%s fnt; %s bri; %s vFnt; %s vBri'\
#        %( len(inBin(mag,zFnt)),len(inBin(mag,zBri)),len(inBin(mag,zVFnt)),len(inBin(mag,zVBri)) )
    # as fn of distance:

    ###oka,okb,dis = spm.match_radec(mgal.ra,mgal.dec,fInfo['cra'],fInfo['cdec'],1.0,nearest=False)

    dra = (gal.ra - fInfo['grpra'])/np.cos(np.deg2rad(gal.dec))
    ddec = gal.dec - fInfo['grpdec']
    dis = np.sqrt(dra**2 + ddec**2)
    
    disMpc = dis*3600.0 / (1.0*1000.0*asperkpc(fInfo['grpz'])).value
    inn = np.where( disMpc <= 0.5)[0]
#    print dis
#    print disMpc
#    print '%s within 0.5 Mpc'%len(inn)
#    print

    # update inMask flag with binary to indicate which number(s) mask in
#    gal.inMask[ok1] = gal.inMask[ok1] + 2**(masknum-1)


def bitMask(x):
    # turn N array of floats (up to 2**(M-1))into
    # into N*M bitmask array 
    strBits = [binary_repr(y,3) for y in x.astype('int')]
    

def writeGMMPS(id,ra,dec,x_ccd,y_ccd,mag,priority):
    #**** NOT WORKING YET ******
    # let's just try a FITS table of the type that's supposed to work:
    """
    The Object Table is a FITS table that contains the following mandatory columns:
    ID    Unique object id (integer)
    RA    RA in hours (real)
    DEC    Dec in degrees (real)
    x_ccd    X coordinate of object position in pixels (real)
    y_ccd    Y coordinate of object position in pixels (real)
    MAG    (Relative) magnitude of object (real)
    priority    Priority of object (char*1; "0/1/2/3/X")
    The order of the columns is important. The column names are case sensitive.
    """
    
    
    nn=len(id)
    out = np.array( [id,ra,dec,x_ccd,y_ccd,mag,priority], \
                      dtype=[('ID',np.int16), ('RA',np.float32), ('DEC',np.float32),('x_ccd',np.float32),\
                             ('y_ccd',np.float32), ('MAG', np.float32), ('priority',np.int16)] )
    
    c1 = Column(name='ID', format='4A', array=id)
    c2 = Column(name='RA', format='E', array=ra)
    c3 = Column(name='DEC', format='E', array=dec)
    c4 = Column(name='x_ccd', format='E', array=x_ccd)
    c5 = Column(name='y_ccd', format='E', array=y_ccd)
    c6 = Column(name='MAG', format='10A', array=mag)
    c7 = Column(name='priority', format='10A', array=priority)

#    coldefs = fits.ColDefs([c1, c2, c3, c4, c5, c6, c7])
#    tbhdu = fits.BinTableHDU.from_columns(coldefs)
#    tbhdu.writeto('test.fits')
    fits.writeto('testODF1.fits',out,clobber=True)


def verifyDes(maskname): # maskname should really be, e.g. bestSpARCS_0035in1.txt, to check the best design of each
    id,xp,yp,ra,dec,wt,slen,swid,pri = np.loadtxt(maskname,unpack=True)
    plt.figure()
    plt.plot(xp,yp,'k,')
    sz=wt*100.+0.1
    plt.scatter(xp,yp,s=sz,color='r',alpha=0.3)

    outname=maskname.replace('in','out')
#    outname=maskname.replace('des','out')
    sra,sdec,sdec,sid = np.loadtxt(outname,unpack=True)

    ok1,ok2=match(id,sid)
    plt.scatter(xp[ok1],yp[ok1],s=sz[ok1],color='b',alpha=0.3)
    plt.show()



def updateinMask(gal,maskfile):
    mra,mdec,mpri,mID = np.loadtxt(maskfile, unpack=True)
    ok1,ok2=match(gal.ID,mID)
    gal.inMask[ok1]=1.0





#def simpCheckColl(x0,xWidth,y0,yWidth,wt,inMask,ss,xobj,yobj,xpad=xpadPix,ypad=ypadPix):
    




#def prepMask(name,nMasks=5,isilent=True):
if(1):    
    
    fInfo = fits.getdata('%sinfo.fits'%name) ; fInfo=fInfo[0] # convert to scalars
    gal = fits.getdata('%sgals.fits'%name)

    stars = fits.getdata('%ssetupCands.fits'%name)

    # CAREFUL: this must only be reset at the start of 
    inMask = np.zeros(len(gal)) # flag whether gal has been assigned to a mask or not:
    prob = np.zeros(len(gal)) # probability of being included in mask
    
    
    # calculate distances:
    disMpc = calcdisMpc(fInfo,gal)
    
    # add all these other arrays to gal array, to make sure everything stays in sync:
    gal = recf.append_fields(gal,('inMask','prob','disMpc'),(inMask,prob,disMpc),asrecarray=True,usemask=False)
    # trim to field:
    inFoV = np.where( (gal.xp>xminField) & (gal.xp<=xmaxField) &  (gal.yp>yminField) & (gal.yp<=ymaxField))[0]
    gal = gal[inFoV]
    
    print '%s gals in full catalogue within imaging field'%(len(gal))
    print

    redSeqCoeffs = np.array((fInfo['RSslope'],fInfo['RSintcpt']))
    redCoeffs = redSeqCoeffs+np.array((0,dRed))
    redLimCoeffs = redSeqCoeffs+np.array((0,dRedLim))


    print
    print '---------- Field info -----------'
    print 'Mask centre     ra,dec = %.6f, %.6f; rotang = %.2f'%(fInfo['cra'],fInfo['cdec'],fInfo['rotang'])
    if (fInfo['grpra']<0.0):
        print '*** using field centre as clus centre ***'
        fInfo['grpra'] = fInfo['cra'] ; fInfo['grpdec'] = fInfo['cdec']
    print 'Cluster centre: ra,dec = %.6f, %.6f; redshift = %.4f'%(fInfo['grpra'],fInfo['grpdec'],fInfo['grpz'])
    print '---------------------------------'
    print


    genWeights(gal,fInfo,scheme='filler')
    
    
#    inAllMasks = np.zeros(len(gal))
    # read in all masks to exclude already allocated objects:

    #**
    #nMasks=1
    #**
    for jj in range(nMasks):        
        inmaskfile = 'best%sout%s.txt'%(name,jj+1)
        # get best (ith) mask     
        mra,mdec,mpri,mID = np.loadtxt(inmaskfile,unpack=True)
        
        oka,okb = match(gal.ID,mID)
        gal.prob[oka] = 0.0 # already in a mask
        # but need to make sure setup stars stay in!
#        inAllMasks[oka]=1.0
    
    # Now, add fillers to each mask:
    for jj in range(nMasks):
        
        
        if(allocateSchemes[jj][0:4]=='band'):
            NSmode='band'
        else:
            NSmode='micro'

        inmaskfile = 'best%sout%s.txt'%(name,jj+1)
        print inmaskfile
        mra,mdec,mpri,mID = np.loadtxt(inmaskfile,unpack=True)

        oka = [i for i,x in enumerate(gal.ID) if x in mID ]

#        oka,okb = match(gal.ID,mID)
        gal.prob[oka] = 1.0
        print '%s must-haves'%len(oka)
        gal.inMask[oka]=1.0
        #print '**',np.sum(gal.inMask)
        # ***stars??
#        sok1,sok2 = match(stars.ID,mID)
        sok1 = [i for i,x in enumerate(stars.ID) if x in mID ]      
        stars = stars[sok1]
#        okst = np.where(mpri==9.0)[0]
#        gal.prob[okb[okst]]=9.0
#        print '%s stars'%len(okst)
        print '%s stars'%len(sok1)
        
        # check that stars aren't being repeated in galaxy cat!:
        sokc = [i for i,x in enumerate(gal.ID) if x in stars.ID ]    
        if(len(sokc)>0):
            gal.prob[sokc]=0.0
#        stop()
#        print '**',np.sum(gal.inMask)
        

#        inname = 'best'+name+'infill%s.txt'%(jj+1)
#        writeInput(inname,gal,fInfo,stars=stars)
#        MD2.desmask( fInfo['cra'],fInfo['cdec'],fInfo['rotang'],inname,niter=nSingleIter ,\
#                     iplot=plotFlag, sortScheme=sortScheme, NSmode=NSmode )


#        infile = inmaskfile.replace('out','in')
        
        #---
        #id,xobj,yobj,ra,dec,wt,slen,swid,stilt = np.loadtxt(inname,unpack=True)
        slen = np.repeat(nomsLen,len(gal))
        xspe,yspe,xToLeft,xToRight,yAbove,yBelow, keepFlag = MD2.defineGMOSspec(gal.xp,gal.yp,slen,gal.prob,NSmode=NSmode)
        # set weight of keepflag==0 objects to 0.0
#        rej=np.where(keepFlag==0.0)[0]
#        if (len(rej)>0): wt[rej]=0.0
        
        x0 = xspe-xToLeft ; xWidth = xToLeft + xToRight
        y0 = yspe-yBelow ; yWidth = yAbove+yBelow
        
        #inMask=np.ones(len(id))
        #zmag=np.repeat(23.,len(id)) #***
        zmag = gal.zmag
        #plt.plot([0,1],[0,1])
        
        xpad=xpadPix
        #**
        ypad=ypadPix+1.0 # extra padding for additional slits
        #ypad=0.0#***
        
        tx0=x0-xpad/2.
        tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
        ty0=y0-ypad/2.
        ty1=y0+yWidth+ypad/2.
        
        
        
        bad= np.where( (tx0<xminSField) | (tx1>xmaxSField) )[0]
#        inMask[bad]=0.0
        gal.prob[bad]=0.0
 
 
        print '**',np.sum(gal.inMask)       
#        bad=np.where(gal.prob==0.0)[0]
#        inMask[bad]=0.0
        
        
        #---
        

        # --
        print 'tot %s'%np.sum(inMask)
        # --- simplified version of resolveCollisions!
        added = np.zeros(len(gal))
        for i in range(len(gal)):
            if(gal.prob[i]==0.0):
                continue
        
            if (gal.inMask[i]==1.0):
                continue # already in mask
            okMask = np.where(gal.inMask>=1.0)[0]
            # if either top or bottom edge of this slit lies within range of an allocated slit, reject:
            olap0 = np.where((ty0[i]>ty0[okMask]) & (ty0[i]<ty1[okMask]) )[0]
            olap1 = np.where((ty1[i]>ty0[okMask]) & (ty1[i]<ty1[okMask]) )[0]
            if ( (len(olap0)>0 ) | (len(olap1)>0) ):
                # reject this slit
        #        print 'rejectd'
                pass
            
            else: 
                print 'added slit!!'
                
                gal.inMask[i]=1.0 # add to mask!
                added[i]=1.0
          
        okMask = np.where(gal.inMask>=1.0)[0]
        

        print len(okMask)

        # --
        aok=np.where(added==1.0)[0]
        print np.transpose((gal.irac1[aok],gal.zmag[aok]-gal.irac1[aok]))
        
        fillname = 'best'+name+'outfill%s.txt'%(jj+1)
        np.savetxt( fillname,np.transpose( (gal.ra[okMask],gal.dec[okMask],gal.prob[okMask],gal.ID[okMask]) ) )
        xtrname = 'best'+name+'extras%s.txt'%(jj+1)
        np.savetxt( xtrname,np.transpose( (gal.ra[aok],gal.dec[aok],gal.prob[aok],gal.ID[aok]) ) )
        
        
        # reset must-haves back to zero for next mask!:
        gal.prob[oka] = 0.0
        gal.inMask[oka] = 0.0
        

    
    
        
    
    
    
    