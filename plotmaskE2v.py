import numpy as np
import matplotlib.pyplot as plt
##from astropy import wcs as pywcs
#import pywcs
#from astrometry.libkd import spherematch as spm
from matplotlib.patches import Rectangle,Circle
import sys,os
from astropy.io import ascii as asciitable
from astropy.io import fits
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

import logging  # only needed for debugging
from glob import glob

# import parameters for Ham detectors:
#from config import *
from configE2v import *

#logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
logging.basicConfig(filename='tmp.log', filemode='w', level=logging.DEBUG)

# Ham: totx =3136




dbglvl = 0
#niter=50



def match(a,b,badval=-1):
    # much faster matching using dictionary!
    good=np.where(b <> badval)[0]
    d=dict(zip(b[good], np.arange(len(b))[good]  ))
    okd = [d.get(x) for x in a]
    # result will be none if there is no match. need to deal
    okddd=np.array(okd)
    ok=np.array(okddd.nonzero()).astype('int64')
    
    okb=(okddd[ok]).astype('int64')
    oka=ok
        
#    return np.array(oka),np.array(okb)
    return np.reshape(np.array(oka),-1),np.reshape(np.array(okb),-1)


def inBin(x,bin):
    ok=np.where( (x>bin[0]) & (x<=bin[1]) )[0]
    return ok


def defineGMOSspec(xobj,yobj,slen,wt,NSmode='micro'):
    # -- Use optical model to obtain wavelength limits (converted to pixels)
    # based on grating and filter selected. 

    # GMOS HAS RED AND BLUE REVERSED FROM DIRECTION GOD INTENDED!!
#    from config import *
    from configE2v import *


    if(NSmode=='band'):
        print 'BAND shuffle'
        # band shuffle instead of micro-shuffle:
        slen=slen/2.0
        yHt=ymaxField-yminField
        yF0=yminField+0.33*yHt
        ymaxField=yminField+0.67*yHt
        yminField = yF0

  

#    # R150, cenwave=850nm, +RG610:
#    cenwave=850 ; linDisp=0.39353344971414417 ; coeff=np.array([-7.78878524e-02,   1.54497292e+02])
#    blueCutA = 610.0
#    redCutA = 1045.0
    
    #Spectrum Length: 1105.3698238764111 (pixels)
    #(1050.-610.)/linDisp = 1118.
    # w. OG515. Spectrum Length: 1346.7724290907997 (pixels)
    #In [77]: (1050-515.)/0.39353344971414417
    #Out[77]: 1359.4778293652412
    # OKAY, actual red cutoff in gmmps seems to be 1045A rather than 1050, so update.
    
    lenRightPix = (cenwave - blueCutA)/linDisp  # REVERSED FOR GMOS!
    lenLeftPix = (redCutA - cenwave)/linDisp 

#    xcen=3000. #**** made-up!
    xcen=ccd_cx #**** made-up!
#    xspec=xobj #(xobj-xcen)+(-0.2)*(xobj-xcen)+xcen # make up anamorphic factor
#    xspec = xobj -np.polyval(coeff,xobj) # measured anamorphic factor  # REVERSED FOR GMOS!
    xspec = xobj +np.polyval(WLcoeff,xobj) # measured anamorphic factor
    yspec=yobj 

    xlo = lenLeftPix * 2.0 # binning
    xhi = lenRightPix * 2.0 # binning
    

    # -- start off with ylo/yhmaxi half slit length
    yhi=slen/2.
    ylo=slen/2.



    # check for objects where spectra fall out of field:
    keepflag=np.ones(len(xobj))
    outy = np.where( (yobj < yminField) | (yobj>ymaxField) )[0]
    keepflag[outy]=0
    xLeft = xspec-xlo
    xRight = xspec+xhi
    outLeft = np.where(xLeft< xminSField)[0]
    keepflag[outLeft]=0 
    outRight = np.where(xRight> xmaxSField)[0] 
    keepflag[outRight]=0 
    
    # also, object must be visible in field. DOH!
    outFoV=np.where( (xobj<xminField) | (xobj>xmaxField) | (yobj<yminField) | (yobj>ymaxField) )[0]    
    keepflag[outFoV]=0
    sStars = np.where(wt==9.0)[0]
    if(len(sStars)>0):
        keepflag[sStars]=1.0 # keep setup stars regardless. does not matter that their spectra fall off array

#    print len(xLeft),' ',len(xlo)
    axlo = np.repeat(xlo,len(xLeft))
    axhi = np.repeat(xhi,len(xRight))
#    print len(axlo)

    return xspec,yspec,axlo,axhi,ylo,yhi, keepflag


def xCMD(id,gal,figDEBUG):
    axCMD = figDEBUG.add_subplot(122)
#    axCMD.plot(mag,col,'k.')
    ok1,ok2 = match(gal.ID,id)
    mag = gal.irac1[ok1]
    col = gal.zmag[ok1] - mag
    
    bad = np.where(col<0.0)[0]
    if (len(bad)>0):
        col[bad]=0.5
    bad = np.where(mag>23.0)[0]
    if (len(bad)>0):
        mag[bad]=23.0
    axCMD.plot(mag,col,'bo',alpha=0.3,ms=15)

    print np.transpose((col,mag))
    plt.show()

def showCMD(id,inMask,col,mag,zmag,figDEBUG):
    axCMD = figDEBUG.add_subplot(122)
    okBri=np.where( (inMask>=1) & (zmag>zBri[0]) & (zmag<=zBri[1]) )[0]
    axCMD.plot(mag,col,'k.')
    axCMD.plot(mag[okBri],col[okBri],'co',ms=10,label='Bri',alpha=0.3)
    okFnt=np.where( (inMask>=1) & (zmag>zFnt[0]) & (zmag<=zFnt[1]) )[0]
    axCMD.plot(mag[okFnt],col[okFnt],'mo',ms=10,label='Fnt',alpha=0.3)

    axCMD.set_xlabel('[3.6]')
    axCMD.set_ylabel('z - [3.6]')
    axCMD.set_xlim([19,23])
#    axCMD.set_ylim([-2,6])
    axCMD.set_ylim([0,4])
    axCMD.legend(numpoints=1)
    plt.show()

# in pixels
def drawMpcCircle(xc,yc,nMpc,fInfo,axname):
    radius = (nMpc*1000.0*asperkpc(fInfo['grpz'])).value # arcsec
    radius = radius/pxscale # pix
    circ = Circle((xc,yc),radius, fill=False, facecolor='none', edgecolor='r')
    axname.add_patch(circ)


def showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,zmag,fInfo,\
    figDEBUG,xpad=0.0,ypad=0.0,colspec='g'):
    tx0=x0-xpad/2.
    tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
    ty0=y0-yWidth/2.
    ty1=y0+yWidth+ypad/2.

    #figDEBUG = plt.figure()
    axDEBUG = figDEBUG.add_subplot(121)
    xCen = (tx0+tx1)/2.0 ; yCen = (ty0+ty1)/2.0 
    #plt.plot(xCen,yCen,'k,') # these points are spectral box centres)
    
    okMask = np.where( inMask==1 )[0]

    # plot objects scaled by weighting:
    
    okBri=np.where( (inMask>=1) & (zmag>zBri[0]) & (zmag<=zBri[1]) )[0]
    okFnt=np.where( (inMask>=1) & (zmag>zFnt[0]) & (zmag<=zFnt[1]) )[0]

    okSS=np.where( (wt==9.0) & (inMask==1.0) )[0]

    for kk in okSS:
        sz = 30.#*wt[kk]
        circ = Circle((xobj[kk],yobj[kk]),sz,color='y',alpha=0.7)
#        plt.text(xobj[kk],yobj[kk],'%.1f'%zmag[kk])
        #plt.text(xobj[kk],yobj[kk],'%.0f'%id[kk])
        #plt.text(xobj[kk],yobj[kk],'%.0f'%kk)
        axDEBUG.add_patch(circ)
            
        rect = Rectangle((x0[kk],y0[kk]),xWidth[kk],yWidth[kk],color='y',ec='none',alpha=0.1)
        axDEBUG.add_patch(rect) 
        # slit:
        sx0=x0-swid/2.+xToLeft
        sx1=swid
        # in wavelength space
        slit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='m',ec='none',alpha=0.2)
        axDEBUG.add_patch(slit)
        # in direct image/mask
        sx0=xobj-swid/2.
        sx1=swid
        rslit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='none',ec='k')
        axDEBUG.add_patch(rslit)



#    ok=np.where( (inMask>=1) & (zmag>zBri[0]))[0]
    for kk in okBri:
        sz = 30.#*wt[kk]
        circ = Circle((xobj[kk],yobj[kk]),sz,color='c',alpha=0.3)
#        plt.text(xobj[kk],yobj[kk],'%.1f'%zmag[kk])
        #plt.text(xobj[kk],yobj[kk],'%.0f'%id[kk])
        #plt.text(xobj[kk],yobj[kk],'%.0f'%kk)
        axDEBUG.add_patch(circ)
            
        rect = Rectangle((x0[kk],y0[kk]),xWidth[kk],yWidth[kk],color=colspec,ec='none',alpha=0.1)
        axDEBUG.add_patch(rect) 
        # slit:
        sx0=x0-swid/2.+xToLeft
        sx1=swid
        # in wavelength space
        slit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='m',ec='none',alpha=0.2)
        axDEBUG.add_patch(slit)
        # in direct image/mask
        sx0=xobj-swid/2.
        sx1=swid
        rslit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='none',ec='k')
        axDEBUG.add_patch(rslit)

        
    """
    bad = np.where(wt==0.0)[0]
    if (len(bad)>0):
        plt.plot(xobj[bad],yobj[bad],'rx')
    """    
#    if (len(okMask)>0):
    
    for kk in okFnt:

#        for kk in okMask:
#            print kk
            # SPECTRUM:
            
            sz = 30.#*wt[kk]
            circ = Circle((xobj[kk],yobj[kk]),sz,color='m',alpha=0.3)
            axDEBUG.add_patch(circ)
            
            rect = Rectangle((x0[kk],y0[kk]),xWidth[kk],yWidth[kk],color='g',ec='none',alpha=0.1)
            axDEBUG.add_patch(rect) 
            # slit:
            sx0=x0-swid/2.+xToLeft
            sx1=swid
            # in wavelength space
            slit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='m',ec='none',alpha=0.2)
            axDEBUG.add_patch(slit)
            # in direct image/mask
            sx0=xobj-swid/2.
            sx1=swid
            rslit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='none',ec='k')
            axDEBUG.add_patch(rslit)
            
            plt.axis('equal')
            plt.plot(xobj,yobj,'k,')
            plt.plot(xobj[okMask],yspe[okMask],'k,')
            plt.xlabel('GMOS x (e2v. binned pix)')
            plt.ylabel('GMOS y (e2v. binned pix)')
        #    if (title):
        #        plt.title(title)
            plt.title('%.0f slits'%np.sum(inMask))
            axDEBUG.set_xlim(-50,3000)
            axDEBUG.set_ylim(-50,2300)

    drawMpcCircle(ccd_cx,ccd_cy,0.5,fInfo,axDEBUG)
    drawMpcCircle(ccd_cx,ccd_cy,1.0,fInfo,axDEBUG)

    drawHam(axDEBUG)
    plt.show()

#*** need to update
def drawHam(ax):
    # draw predicted Hamamatsu edges relative to E2V (assuming same centres:

    cenXshift=0.0

    rect=Rectangle((0+cenXshift,0),ccd_dx,ccd_dy,color='none',ec='r')
    ax.add_patch(rect)
    rect=Rectangle((0+ccd_dx+ccd_xgap+cenXshift,0),ccd_dx,ccd_dy,color='none',ec='r')
    ax.add_patch(rect)
    rect=Rectangle((0+2.*(ccd_dx+ccd_xgap)+cenXshift,0),ccd_dx,ccd_dy,color='none',ec='r')
    ax.add_patch(rect)

    # GMOS imaging field:
    plt.plot((x0,x0),(y0,y1),'k-',lw=2)
    plt.plot((x0,x1),(y1,y1),'k-',lw=2)
    plt.plot((x1,x1),(y1,y0),'k-',lw=2)
    plt.plot((x1,x0),(y0,y0),'k-',lw=2)

def qshowSlits(id,gal,fInfo,figDEBUG,xpad=xpadPix,ypad=ypadPix,NSmode='micro',slitcol='g'):

    ok1,ok2 = match(id,gal.ID)

    prob = np.ones(len(ok2))
    inMask = np.ones(len(ok2))

    slen = np.repeat(nomsLen,len(ok2))
    swid = np.repeat(nomsWid,len(ok2))
    xspe,yspe,xToLeft,xToRight,yAbove,yBelow, keepFlag = defineGMOSspec(gal.xp[ok2],gal.yp[ok2],slen,prob,NSmode=NSmode)

    x0 = xspe-xToLeft ; xWidth = xToLeft + xToRight
    y0 = yspe-yBelow ; yWidth = yAbove+yBelow


    
    showSlits(id,gal.xp[ok2],gal.yp[ok2],yspe,x0,xToLeft,xWidth,y0,yWidth,prob,inMask,\
              swid,slen,gal.zmag[ok2],fInfo,figDEBUG,xpad=xpad,ypad=ypad,colspec=slitcol)





from glob import glob
def plotmask(infile,galfile,NSmode='micro'):
    figDEBUG = plt.figure()
    
    gal = fits.getdata(galfile+'gals.fits')
    fInfo = fits.getdata(galfile+'info.fits') ; fInfo=fInfo[0]
 
    
 
    # NOTE: rotang is not actually needed for GoGREEN GMOS masks, since we're designing from pixel coords directly
 
    #(cra,cdec,rotang,infile) = (150.5702, 2.4986000000000002, 0.0, 'test1in.txt')
    #(cra,cdec,rotang,infile) = (150.5702, 2.4986000000000002, 0.0, 'COS221in1.txt')
    
    infiles=glob(infile)
    if (len(infiles)>1):
        for tinfile in infiles:
            tt = np.loadtxt(tinfile,unpack=False)
            try: 
                cdat = np.vstack((cdat,tt))
            except:
                cdat = tt
        u,ss = np.unique(cdat[:,0],return_index=True)
        print len(cdat)
        print len(ss)
        id = cdat[ss,0]
        xobj = cdat[ss,1]
        yobj = cdat[ss,2]
        ra = cdat[ss,3]
        dec = cdat[ss,4]
        wt = cdat[ss,5]
        slen = cdat[ss,6]
        swid = cdat[ss,7]
        stilt = cdat[ss,8]
        #id,xobj,yobj,ra,dec,wt,slen,swid,stilt = np.loadtxt(infile,unpack=True)

    else:
        id,xobj,yobj,ra,dec,wt,slen,swid,stilt = np.loadtxt(infile,unpack=True)
    
    ## load setup stars and add to arrays:
#    if (starfile):
#        id,xobj,yobj,ra,dec,wt,slen,swid,stilt = getStars(starfile,id,xobj,yobj,ra,dec,wt,slen,swid,stilt)
    
    
    xspe,yspe,xToLeft,xToRight,yAbove,yBelow, keepFlag = defineGMOSspec(xobj,yobj,slen,wt,NSmode=NSmode)
    # set weight of keepflag==0 objects to 0.0
    rej=np.where(keepFlag==0.0)[0]
    if (len(rej)>0): wt[rej]=0.0
    
    x0 = xspe-xToLeft ; xWidth = xToLeft + xToRight
    y0 = yspe-yBelow ; yWidth = yAbove+yBelow


    outfile = infile.replace('in','out')
    outfiles = glob(outfile)
    if (len(outfiles)>1):
        for toutfile in outfiles:
            tt = np.loadtxt(toutfile,unpack=False)
            try: 
                odat = np.vstack((odat,tt))
            except:
                odat = tt
        uu,sss = np.unique(odat[:,3],return_index=True)
        print len(odat)
        print len(sss)
        mra = odat[sss,0]
        mID = odat[sss,3]
    else:
        mra,mdec,mpri,mID = np.loadtxt(outfile,unpack=True)
    
    ok1,ok2 = match(id,mID)
    
    oka,okb = match(id,gal.ID)
    zmag = np.zeros(len(id))
    inMask = np.zeros(len(id)) ; inMask[ok1]=1.0
    zmag[oka] = gal.zmag[okb]
    
    col = np.zeros(len(id)) ; mag = np.zeros(len(id))
    col[oka] = gal.zmag[okb]-gal.irac1[okb]
    mag[oka] = gal.irac1[okb]
    
    showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,zmag,fInfo,figDEBUG,xpad=xpadPix,ypad=ypadPix)
    showCMD(id,inMask,col,mag,zmag,figDEBUG)
    
    
    #--
    xtrfile=infile.replace('in','extras')
    """
    if (glob(xtrfile)):
        print 'found extras'
        mra,mdec,mpri,mID = np.loadtxt(xtrfile,unpack=True)
        gok1,gok2 = match(gal.ID,mID)
        xspe,yspe,xToLeft,xToRight,yAbove,yBelow, keepFlag = defineGMOSspec(gal.xp[gok1],gal.yp[gok1],\
                                                                            np.repeat(nomsLen,len(gok1)),\
                                                                            np.repeat(1.0,len(gok1)),NSmode=NSmode)
                                                                            #gal.prob[gok1],NSmode=NSmode)
        x0 = xspe-xToLeft ; xWidth = xToLeft + xToRight
        y0 = yspe-yBelow ; yWidth = yAbove+yBelow

        
        print len(ok1)
        inMask = np.zeros(len(id)) ; inMask[ok1]=1.0
        showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,zmag,fInfo,\
                  figDEBUG,xpad=xpadPix,ypad=ypadPix,colspec='b')
    """
    outpng = outfile.replace('.txt','check.png')

    try:
        xmra,xmdec,xmpri,xmID = np.loadtxt(xtrfile,unpack=True)
    #    plt.close('all')
    #    figDEBUG=plt.figure()
        qshowSlits(xmID,gal,fInfo,figDEBUG,xpad=xpadPix,ypad=ypadPix,NSmode=NSmode,slitcol='b')
        xCMD(xmID,gal,figDEBUG)
        #--
        outpng = outpng.replace('.png','_xtra.png')
    except:
        print 'no extra slits'
    
    if (len(outfiles)>1):
        plt.title('%s\n%.0f masks'%(outfile,len(outfiles)))
        outpng = outpng.replace('*','All')
        outpng = outpng.replace('?','All')
    else:
        plt.title('%s'%(outfile))
        
    plt.savefig(outpng)

