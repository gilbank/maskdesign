import numpy as np
import matplotlib.pyplot as plt
##from astropy import wcs as pywcs
#import pywcs
#from astrometry.libkd import spherematch as spm
from matplotlib.patches import Rectangle,Circle
import sys,os
from astropy.io import ascii as asciitable
from astropy.io import fits
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

import logging  # only needed for debugging

# import parameters for Ham detectors:


#import maskdesHam as MD2
import maskdesE2v as MD2

#from config import *
#infile = 'bestSPT-0205infill2.txt' 
#outfile = 'bestSPT-0205out2.txt' 
#galfile='SPT-0205'

from configE2v import *
infile = 'bestSXDF76infill2.txt' 
outfile = 'bestSXDF76out2.txt' 
galfile='SXDF76'


NSmode='micro'

figDEBUG = plt.figure()
slitcol='g'


gal = fits.getdata(galfile+'gals.fits')
fInfo = fits.getdata(galfile+'info.fits') ; fInfo=fInfo[0]



def defineGMOSspec(xobj,yobj,slen,wt,NSmode='micro'):
    # -- Use optical model to obtain wavelength limits (converted to pixels)
    # based on grating and filter selected. 

    # GMOS HAS RED AND BLUE REVERSED FROM DIRECTION GOD INTENDED!!
    from config import *
#    from configE2v import *


    if(NSmode=='band'):
        print 'BAND shuffle'
        # band shuffle instead of micro-shuffle:
        slen=slen/2.0
        yHt=ymaxField-yminField
        yF0=yminField+0.33*yHt
        ymaxField=yminField+0.67*yHt
        yminField = yF0

  

#    # R150, cenwave=850nm, +RG610:
#    cenwave=850 ; linDisp=0.39353344971414417 ; coeff=np.array([-7.78878524e-02,   1.54497292e+02])
#    blueCutA = 610.0
#    redCutA = 1045.0
    
    #Spectrum Length: 1105.3698238764111 (pixels)
    #(1050.-610.)/linDisp = 1118.
    # w. OG515. Spectrum Length: 1346.7724290907997 (pixels)
    #In [77]: (1050-515.)/0.39353344971414417
    #Out[77]: 1359.4778293652412
    # OKAY, actual red cutoff in gmmps seems to be 1045A rather than 1050, so update.
    
    lenRightPix = (cenwave - blueCutA)/linDisp  # REVERSED FOR GMOS!
    lenLeftPix = (redCutA - cenwave)/linDisp 

#    xcen=3000. #**** made-up!
    xcen=ccd_cx #**** made-up!
#    xspec=xobj #(xobj-xcen)+(-0.2)*(xobj-xcen)+xcen # make up anamorphic factor
#    xspec = xobj -np.polyval(coeff,xobj) # measured anamorphic factor  # REVERSED FOR GMOS!
    xspec = xobj +np.polyval(WLcoeff,xobj) # measured anamorphic factor
    yspec=yobj 

    xlo = lenLeftPix * 2.0 # binning
    xhi = lenRightPix * 2.0 # binning
    

    # -- start off with ylo/yhmaxi half slit length
    yhi=slen/2.
    ylo=slen/2.



    # check for objects where spectra fall out of field:
    keepflag=np.ones(len(xobj))
    outy = np.where( (yobj < yminField) | (yobj>ymaxField) )[0]
    keepflag[outy]=0
    xLeft = xspec-xlo
    xRight = xspec+xhi
    outLeft = np.where(xLeft< xminSField)[0]
    keepflag[outLeft]=0 
    outRight = np.where(xRight> xmaxSField)[0] 
    keepflag[outRight]=0 
    
    # also, object must be visible in field. DOH!
    outFoV=np.where( (xobj<xminField) | (xobj>xmaxField) | (yobj<yminField) | (yobj>ymaxField) )[0]    
    keepflag[outFoV]=0
    sStars = np.where(wt==9.0)[0]
    if(len(sStars)>0):
        keepflag[sStars]=1.0 # keep setup stars regardless. does not matter that their spectra fall off array

#    print len(xLeft),' ',len(xlo)
    axlo = np.repeat(xlo,len(xLeft))
    axhi = np.repeat(xhi,len(xRight))
#    print len(axlo)

    return xspec,yspec,axlo,axhi,ylo,yhi, keepflag

def showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,zmag,fInfo,figDEBUG,xpad=0.0,ypad=0.0,slitcol='g',alpha=0.1):
    tx0=x0-xpad/2.
    tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
    ty0=y0-ypad/2.
    ty1=y0+yWidth+ypad/2.

    #figDEBUG = plt.figure()
#    axDEBUG = figDEBUG.add_subplot(121)
    axDEBUG = figDEBUG.add_subplot(111)
    xCen = (tx0+tx1)/2.0 ; yCen = (ty0+ty1)/2.0 
    #plt.plot(xCen,yCen,'k,') # these points are spectral box centres)
    
    okMask = np.where( inMask==1 )[0]

    # plot objects scaled by weighting:
    
    #okBri=np.where( (inMask>=1) & (zmag>zBri[0]) & (zmag<=zBri[1]) )[0]
    # no mag seln:
    okBri=np.where( (inMask>=1)  )[0]
#    okFnt=np.where( (inMask>=1) & (zmag>zFnt[0]) & (zmag<=zFnt[1]) )[0]

    okSS=np.where( (wt==9.0) & (inMask==1.0) )[0]

    for kk in okSS:
        sz = 30.#*wt[kk]
        circ = Circle((xobj[kk],yobj[kk]),sz,color='y',alpha=0.7)
#        plt.text(xobj[kk],yobj[kk],'%.1f'%zmag[kk])
        #plt.text(xobj[kk],yobj[kk],'%.0f'%id[kk])
        #plt.text(xobj[kk],yobj[kk],'%.0f'%kk)
        axDEBUG.add_patch(circ)
            
        rect = Rectangle((x0[kk],y0[kk]),xWidth[kk],yWidth[kk],color='y',ec='none',alpha=alpha)
        axDEBUG.add_patch(rect) 
        # slit:
        sx0=x0-swid/2.+xToLeft
        sx1=swid
        # in wavelength space
        slit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='m',ec='none',alpha=0.2)
        axDEBUG.add_patch(slit)
        # in direct image/mask
        sx0=xobj-swid/2.
        sx1=swid
        rslit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='none',ec='k')
        axDEBUG.add_patch(rslit)



#    ok=np.where( (inMask>=1) & (zmag>zBri[0]))[0]
    for kk in okBri:
        sz = 30.#*wt[kk]
        circ = Circle((xobj[kk],yobj[kk]),sz,color='c',alpha=0.3)
#        plt.text(xobj[kk],yobj[kk],'%.1f'%zmag[kk])
        #plt.text(xobj[kk],yobj[kk],'%.0f'%id[kk])
        #plt.text(xobj[kk],yobj[kk],'%.0f'%kk)
        plt.text(xobj[kk],yobj[kk],'%.3f'%wt[kk])
        axDEBUG.add_patch(circ)
            
        #print (x0[kk],y0[kk]),xWidth[kk],yWidth[kk]
        rect = Rectangle((x0[kk],y0[kk]),xWidth[kk],yWidth[kk],color=slitcol,ec='none',alpha=alpha)
        axDEBUG.add_patch(rect) 
        # slit:
        sx0=x0-swid/2.+xToLeft
        sx1=swid
        # in wavelength space
        slit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='m',ec='none',alpha=0.2)
        axDEBUG.add_patch(slit)
        # in direct image/mask
        sx0=xobj-swid/2.
        sx1=swid
        rslit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='none',ec='k')
        axDEBUG.add_patch(rslit)
        plt.show()
        
    """
    bad = np.where(wt==0.0)[0]
    if (len(bad)>0):
        plt.plot(xobj[bad],yobj[bad],'rx')
    """    
#    if (len(okMask)>0):
    
    """
    for kk in okFnt:

#        for kk in okMask:
#            print kk
            # SPECTRUM:
            
            sz = 30.#*wt[kk]
            circ = Circle((xobj[kk],yobj[kk]),sz,color='m',alpha=0.3)
            axDEBUG.add_patch(circ)
            
            rect = Rectangle((x0[kk],y0[kk]),xWidth[kk],yWidth[kk],color=slitcol,ec='none',alpha=0.1)
            axDEBUG.add_patch(rect) 
            # slit:
            sx0=x0-swid/2.+xToLeft
            sx1=swid
            # in wavelength space
            slit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='m',ec='none',alpha=0.2)
            axDEBUG.add_patch(slit)
            # in direct image/mask
            sx0=xobj-swid/2.
            sx1=swid
            rslit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='none',ec='k')
            axDEBUG.add_patch(rslit)
            
            plt.axis('equal')
            plt.plot(xobj,yobj,'k,')
            plt.plot(xobj[okMask],yspe[okMask],'k,')
            plt.xlabel('GMOS x (Ham. binned pix)')
            plt.ylabel('GMOS y (Ham. binned pix)')
        #    if (title):
        #        plt.title(title)
            plt.title('%.0f slits'%np.sum(inMask))
            axDEBUG.set_xlim(-50,3000)
            axDEBUG.set_ylim(-50,2300)

    #drawMpcCircle(ccd_cx,ccd_cy,0.5,fInfo,axDEBUG)
    #drawMpcCircle(ccd_cx,ccd_cy,1.0,fInfo,axDEBUG)
    #
    #drawHam(axDEBUG)
    """
    plt.show()



id,xobj,yobj,ra,dec,wt,slen,swid,stilt = np.loadtxt(infile,unpack=True)
xspe,yspe,xToLeft,xToRight,yAbove,yBelow, keepFlag = defineGMOSspec(xobj,yobj,slen,wt,NSmode=NSmode)
# set weight of keepflag==0 objects to 0.0
rej=np.where(keepFlag==0.0)[0]
if (len(rej)>0): wt[rej]=0.0

x0 = xspe-xToLeft ; xWidth = xToLeft + xToRight
y0 = yspe-yBelow ; yWidth = yAbove+yBelow

inMask=np.ones(len(id))
zmag=np.repeat(23.,len(id)) #***
#plt.plot([0,1],[0,1])

xpad=xpadPix
#**
ypad=ypadPix+1.0 # extra padding for additional slits
#ypad=0.0#***

tx0=x0-xpad/2.
tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
ty0=y0-ypad/2.
ty1=y0+yWidth+ypad/2.



bad= np.where( (tx0<xminSField) | (tx1>xmaxSField) )[0]
inMask[bad]=0.0

bad=np.where(wt==0.0)[0]
inMask[bad]=0.0



showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,zmag,fInfo,figDEBUG,xpad=xpadPix,ypad=ypadPix,slitcol='g')
#plt.plot([0,1],[0,1])






mra,mdec,mpri,mID = np.loadtxt(outfile,unpack=True)
from match import match
oka,okb = match(id,mID)
inMask[:]=0.0
inMask[oka]=1.0


#*** stars?
showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,zmag,fInfo,figDEBUG,xpad=xpadPix,ypad=ypadPix,slitcol='k',alpha=1.0)
plt.show()
#plt.plot([0,1],[0,1])
plt.xlim([-200,4200])
plt.ylim([0,2700])
#plt.plot([0,1],[0,1])

plt.title(infile)
plt.show()


okMask = np.where(inMask >=1.0)[0]
print len(okMask)
ss = np.argsort(np.random.random(len(wt))) # just randomise order
inMaskX = MD2.resolveCollisions(x0,xWidth,y0,yWidth,wt,inMask,ss,xobj,yobj,xpad=xpadPix,ypad=ypadPix)
okMaskX = np.where(inMaskX >=1.0)[0]
print len(okMaskX)

plt.axvline(xminSField,color='r')
plt.axvline(xmaxSField,color='r')


plt.show()


"""
for ii in ty0:
    plt.axhline(ii,c='g')
for ii in ty1:
    plt.axhline(ii,c='r')
""" 

# try to add:
#notinMask = np.where(inMask==0.0)[0]
# --- simplified version of resolveCollisions!
added = np.zeros(len(id))
for i in range(len(id)):
    if(wt[i]==0.0):
        continue

    if (inMask[i]==1.0):
        continue # already in mask
    okMask = np.where(inMask>=1.0)[0]
    # if either top or bottom edge of this slit lies within range of an allocated slit, reject:
    olap0 = np.where((ty0[i]>ty0[okMask]) & (ty0[i]<ty1[okMask]) )[0]
    olap1 = np.where((ty1[i]>ty0[okMask]) & (ty1[i]<ty1[okMask]) )[0]
    if ( (len(olap0)>0 ) | (len(olap1)>0) ):
        # reject this slit
#        print 'rejectd'
        pass
    
    else: 
        print 'added slit!!'
#        inMask[i]=1.0 # add to mask!
        added[i]=1.0
  
okMask = np.where(inMask>=1.0)[0]
# ---
print len(okMask)
showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,zmag,fInfo,figDEBUG,xpad=xpadPix,ypad=ypadPix,slitcol='b',alpha=0.5)
plt.show()

addedoutfile = infile.replace('in','out')

np.savetxt(addedoutfile,np.transpose((ra[okMask],dec[okMask],wt[okMask],id[okMask])))





