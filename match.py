import numpy as np


def match(a,b,badval=-1):
    # much faster matching using dictionary!
    good=np.where(b <> badval)[0]
    d=dict(zip(b[good], np.arange(len(b))[good]  ))
#    d=dict(zip(b, range(len(b))  ))
    okd = [d.get(x) for x in a]
    # result will be none if there is no match. need to deal
    okddd=np.array(okd)
    ok=np.array(okddd.nonzero()).astype('int64')
    
    okb=(okddd[ok]).astype('int64')
    oka=ok
    
    
    
#    return np.array(oka),np.array(okb)
    return np.reshape(np.array(oka),-1),np.reshape(np.array(okb),-1)

    """
    # this might be the really neat way of doing it:
    tok = [d.get(x) for x in gg.sdssid]
    ok1 = [x for x in tok if x is not None]
    ok2 = [i for i,x in enumerate(tok) if x is not None]
    """

def simpmatch(a,b):
    # much faster matching using dictionary!
    #good=np.where(b <> badval)[0]
    d=dict(zip(b, np.arange(len(b))  ))
#    d=dict(zip(b, range(len(b))  ))
    okd = [d.get(x) for x in a]
    # result will be none if there is no match. need to deal
    okddd=np.array(okd)
    ok=np.array(okddd.nonzero()).astype('int64')
    
    okb=(okddd[ok]).astype('int64')
    oka=ok
    
    
    
#    return np.array(oka),np.array(okb)
    return np.reshape(np.array(oka),-1),np.reshape(np.array(okb),-1)

    
    """
ddd=dict(zip(mm.OBJID,range(len(mm))))
okdd = [ddd.get(x) for x in fff.ObjID]
#ffnuv=gg.mag_nuv[okdd]
#ffchk=gg.checkSDSSid[okdd]
okddd=np.array(okdd)
ok=np.array(okddd.nonzero()).astype('int64')
okbg=(okddd[ok]).astype('int64')
okag=ok

fffchk=-np.ones(len(fff)).astype('int64') # this must be done here first (or flat-->int64 rounding error will screw things!)
#fffchk[okag]=mm.OBJID[okbg]

fffMatchFullIndex = -np.ones(len(fff)).astype('int64')
fffMatchFullIndex[okag]=okbg

# **** CAN USE THE FOLLOWING TO RECOVER ANY PARAMETER FROM masttab_full.fits from SDSS-selected a40 sample ****
fffchk = mm.OBJID[fffMatchFullIndex] # Need to be careful about -1 values! it will go to penultimate element!
    """
def OLDmatch(a,b):
    
    ok1=[]
    ok2=[]
    for i,item in enumerate( b ):
    
        tok1=np.where(item==a)[0]
        if (len(tok1)==0): continue
        #ok1=np.append(ok1,tok1)
        #ok2=np.append(ok2,i)
        ok1.append(tok1[0])
        ok2.append(i)

        
#        print  a[ok1],b[ok2]
    return np.array(ok1),np.array(ok2)