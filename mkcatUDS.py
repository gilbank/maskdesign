from astropy.io import fits,ascii
from astropy import wcs

import numpy as np
import matplotlib.pyplot as plt

from numpy.lib import recfunctions as recf
#from maskdes import *
from astropy.coordinates import SkyCoord
import astropy.units as u
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

from config import *

# COSMOS cat:
#dat = fits.getdata('/home/gilbank/Proj/goGreen/PubCats/photoz_zml_vers1.8_250909.fits')
# ULTRAVISTA cat:
#dat = ascii.read('/home/gilbank/Proj/goGreen/PubCats/UVISTA_final_v4.1.cat')
# http://www.strw.leidenuniv.nl/galaxyevolution/ULTRAVISTA/Ultravista/K-selected.html


#def ab2Jy(mag):
#        foo = 23.0 - ((mag+48.6)/2.5)
#        return 10**foo

#def eab2eJy(flux,emag,minerr=0.01):
#        #ab=f2m(x)
#        #eab=(2.5/alog(10.))*ex/x
#        temag = np.sqrt(emag**2 + minerr**2)
#        eflux = temag * flux *np.log(10.0)/2.5 #/ (2.5/np.log(10.0))
#        
# 
#        return eflux

"""
def match(a,b,badval=-1):
    # much faster matching using dictionary!
    good=np.where(b <> badval)[0]
    assert len(good) > 0
    d=dict(zip(b[good], np.arange(len(b))[good]  ))
    okd = [d.get(x) for x in a]
    # result will be none if there is no match. need to deal
    okddd=np.array(okd)
    ok=np.array(okddd.nonzero()).astype('int64')
    
    okb=(okddd[ok]).astype('int64')
    oka=ok
""" 

def getXY(id,GMIdat):
    print 'USING gmskcreate e2v coords'
    s = map(float,GMIdat['ID'])
    Gid = np.array(s)
#    print id
#    print Gid   
#    ok1,ok2 = match(id,Gid)

    d=dict(zip(Gid, np.arange(len(Gid))  ))

    okd = [d.get(x) for x in id]
    okddd=np.array(okd)
    ok=np.array(okddd.nonzero()).astype('int64')
    ok2=(okddd[ok]).astype('int64')
    ok1=ok

    xp = -np.zeros(len(id))-999.0
    yp = -np.zeros(len(id))-999.0
    
    xp[ok1] = GMIdat['x_ccd'][ok2] / 2.0 # binning!
    yp[ok1] = GMIdat['y_ccd'][ok2] / 2.0
    
    # CHECKED. MATCHING IS CORRECT. ra,dec agree to within (tiny) rounding error
    
    print yp.min()
###    assert min(yp)>-1.0
    
    return xp,yp

def flux2ab(flux):
    ok = np.where(flux>0.0)[0]
    ab = np.zeros(len(flux))+99.0
    ab[ok] =  -2.5*np.log10(flux[ok])+25.0
    return ab

def eflux2eab(eflux,flux,minerr=0.01):
    ok = np.where(flux>0.0)[0]
    eab = np.zeros(len(eflux))+99.0
    eab[ok] = eflux[ok] *2.5/flux[ok]/np.log(10)
    return np.sqrt(eab**2 + minerr**2)


def getZphWts(gal,fInfo):
        phzWt = np.zeros(len(gal))
        # -- 2nd highest prioirty photoz (within: 0.7--2.0):
        ok = np.where( (gal.zph>0.7) & (gal.zph<2.0) )[0]
        #sel2 = np.where( (dat.zPDF>0.7) & (dat.zPDF<2.0) )[0]
        phzWt[ok] = 0.5
        
        # -- highest priority photo-z (within 2sigma of grpz):
        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph >= fInfo['grpz']) )[0]
#        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph>=grpz), 1.0, 0.0 )[0]
        #   sel = np.where( (dat.zPDF>0) & (2.0*dat.zPDF_l68-dat.zPDF <= grpz) & (2.*dat.zPDF_u68-dat.zPDF>=grpz) & (dat.TYPE==0) )[0]
        phzWt[ok] =1.0
        # *** NOTE: gals with phzs outside this range (or bad photozs) are hence completely rejected! ***
        return phzWt



def crudeSky2xy(cra,cdec,rotang,ra,dec):
    print '\n*** WARNING: only doing crude conversion to pixel coords. Use IRAF Gemini task for final cats ***\n'
    w = wcs.WCS(naxis=2)
    w.wcs.crpix = [ccd_cx,ccd_cy] 

    print ccd_cx,ccd_cy
    print cra,cdec

    #wcs.wcs.cdelt = np.array([-pxscale, pxscale])
    #wcs.wcs.cdelt = np.array([-pxscale, pxscale])/3600.
    
    w.wcs.crval = [cra, cdec]
    w.wcs.ctype = ["RA---TAN", "DEC--TAN"]
    #wcs.wcs.crota = [rotang, rotang] #** CHECK **
    # [for some reason, setting the rotval keyword no longer seems to work, so manipulate CD matrix directly instead!
    
    theta_rad=np.deg2rad(rotang[0])  #*** need to CHECK sign!
    cd = [ (-pxscale*np.cos(theta_rad)/3600.0,-pxscale*np.sin(theta_rad)/3600.0), \
            (-pxscale*np.sin(theta_rad)/3600.0,pxscale*np.cos(theta_rad)/3600.0)]
    w.wcs.cd = cd
#    wcs.wcs.print_contents()

#    xpix,ypix = wcs.wcs_world2pix(ra,dec, 1)  
    xpix,ypix = w.wcs_world2pix(ra,dec, 1)  
    return xpix,ypix


    """
    gmskcreate ("cos221.rd",
    " GN-2007A-Q-4", "gmos-s", 150.5702, 2.4986, 90., fl_getim=no, inimage="",
    outimage="", fl_getxy=yes, outcoords="coss21.txt", outtab="coss21OT",
    iraunits="degrees", fraunits="degrees", slitszx=1., slitszy=5.,
    logfile="logfile", glogpars="", fl_inter=yes, verbose=yes, fl_debug=no,
    status=0, scanfile="")
    """


def mkcatUDS(name,dcol=0.0):

    #name = 'SXDF64'#XGG'
    
    
#    if (name=='COS-221'):
##        dat = ascii.read('/home/gilbank/Proj/goGreen/PubCats/UVISTA_final_v4.1.cat')
#        dat = fits.getdata('/home/gilbank/Proj/goGreen/PubCats/UVISTA_final_v4.1.fits.gz')
#        zdat = fits.getdata('/home/gilbank/Proj/goGreen/PubCats/UVISTA_final_v4.1_zout.fits')
#        # http://www.strw.leidenuniv.nl/galaxyevolution/ULTRAVISTA/Ultravista/K-selected.html
#        dat = recf.append_fields(dat,('z_p','l68','u68'),(zdat['z_p'],zdat['l68'],zdat['u68']))
#    else:    
    if(1):
        rootdir='/home/gilbank/Proj/goGreen/'
        catfits='%s/PubCats/UDS_DR8/uds8_v0.2.test.fits'%rootdir
        dat = fits.getdata(catfits)
        GMIdat = fits.getdata('%sMaskDesign/e2v/%s_GMI.fits'%(rootdir,name))

    # from wiki:
    
    """
    COSMOS-221 
    150.5702 
    2.4986 
    1.146 
    no 
    RIzJHK 
    """
    
    mm = fits.getdata('%sMaskDesign/maskinfo.fits'%rootdir)
    ok=np.where(mm.Name==name)[0]
    #name='COS221'
    rotang = mm['PA'][ok]
    ccoo= SkyCoord(mm['cra'][ok],mm['cdec'][ok],unit=(u.hour,u.deg))
    cra,cdec = ccoo.ra.value[0],ccoo.dec.value[0]
    grpcoo= SkyCoord(mm['grpra'][ok],mm['grpdec'][ok],unit=(u.hour,u.deg))
    grpra,grpdec = grpcoo.ra.value[0],ccoo.dec.value[0]
    grpz=mm['grpz'][ok]
    
    #grpdec=-5.2
    
    print grpra,grpdec,grpz
    
    #rotang=90.
    #cra = 150.5702 ; cdec=2.4986 ; grpz=1.146 
    #grpra,grpdec = -1.,-1. 
    
    # nominal RS fit:
    ##--- by eye
    x0,y0=20.15,2.06 ; x1,y1=21.83,1.86
    #rslope=(y1-y0)/(x1-x0)
    rslope = 0.0#***
    rintcpt=y0-rslope*x0 +0.35+0.2+0.2 + dcol
    redSeqCoeffs = np.array((rslope,rintcpt))
    redCoeffs = redSeqCoeffs+np.array((0,dRed))
    
    
    # make universal format for all cats.
    
    # gals only:
    # id,ra,dec,irac1,zmag,eirac1,ezmag,
    # optional: iphotoz?
    # photoz, ezph0, ezph1, zspec?
    
    # field info:
    # cra,cdec,rotang,
    
    # clus/group info:
    # grpra,grpdec,cz
    
    # Setup stars:
    # id,ra,dec,zmag
    
    
    # Field Info:
    #fInfo = np.recarray(cra, dtype=['cra'])
    t = np.recarray(0,formats=('float'))
    fInfo = recf.append_fields(t,('cra','cdec','rotang','grpra','grpdec','grpz','RSslope','RSintcpt'),\
                               (cra,cdec,rotang,grpra,grpdec,grpz,rslope,rintcpt),\
                               asrecarray=True,usemask=False)
    fInfo = recf.drop_fields(fInfo,'f0',asrecarray=True,usemask=False)
    #fInfo=fInfo[0]
    
    id = dat['id']
    ra = dat['ra']
    dec = dat['dec']
#    irac1 = -2.5*np.log10(dat['ch1_colf'])+25.0
    irac1 = flux2ab(dat['ch1_colf'])
    #eirac1 = dat['ch1_colfe']#***
    eirac1 = eflux2eab(dat['ch1_colfe'],dat['ch1_colf'])
#    zmag = -2.5*np.log10(dat['z_colf'])+25.0
    zmag = flux2ab(dat['z_colf'])
#    ezmag = dat['z_colfe']#***
    ezmag = eflux2eab(dat['z_colfe'],dat['z_colf'])
    
#    bad=np.where(dat['ch1_colf']<=0.0)[0]
#    if(len(bad)>0): irac1[bad]=0.0
#    bad=np.where(dat['z_colf']<=0.0)[0]
#    if(len(bad)>0): zmag[bad]=0.0
    
    Kstot = flux2ab(dat['K_totf'])
#    Kstot = -2.5*np.log10(dat['K_totf'])+25.0
#    bad=np.where(dat['K_totf']<=0.0)[0]
#    if(len(bad)>0): Kstot[bad]=0.0
 
    Kap = flux2ab(dat['K_colf'])
#    Kap = -2.5*np.log10(dat['K_colf'])+25.0
#    bad=np.where(dat['K_colf']<=0.0)[0]
#    if(len(bad)>0): Kap[bad]=0.0
    
    # convert both ap mags to total
    irac1 = irac1-Kap +Kstot
    zmag = zmag-Kap+Kstot
    
    #zdat = ascii.read('/home/gilbank/Proj/goGreen/PubCats/UVISTA_final_v4.1.zout')
    zdat = fits.getdata('%s/PubCats/UDS_DR8/udsz.fits'%rootdir)
    
    ###zph = zdat['z_a'] #*** is this best choice???
    zph = zdat['z_p'] #*** is this best choice???
    zphLo = zdat['l68']
    zphHi = zdat['u68']
###    izph = True
    izph = np.ones(len(dat))
    #izs = False
    
    zs = zdat['z_spec'] #-np.ones(len(id))
    #***
    #zsq = -np.ones(len(id)) # zs quality flag ACTUALLY LET'S JUST ASSUME THE MASK DESIGN CODE ONLY GETS GIVEN GOOD ZSs
    #    xp,yp = MaskpixfromSky(fInfo['cra'][0],fInfo['cdec'][0],fInfo['rotang'][0],obj.ra,obj.dec)
#    xp,yp = crudeSky2xy(cra,cdec,rotang,ra,dec)
    xp,yp = getXY(id,GMIdat)

#    obj = recf.append_fields(t,('ID','ra','dec','irac1','eirac1','zmag','ezmag','zph','zphLo','zphHi','izph','zs','xpHam','ypHam'),\
    obj = recf.append_fields(t,('ID','ra','dec','irac1','eirac1','zmag','ezmag','zph','zphLo','zphHi','izph','zs','xp','yp'),\
                             (id,ra,dec,irac1,eirac1,zmag,ezmag,zph,zphLo,zphHi,izph,zs,xp,yp),\
                               asrecarray=True,usemask=False)
    
    obj = recf.drop_fields(obj,'f0',asrecarray=True,usemask=False)
    
    # TRIM FIELD
    
    """
    yminField = 70.0
    ymaxField = 2247.0
    xminField = 450.0 # imaging field
    xmaxField = 2660.0
    """
    # let's go with wider cuts so the field can be recentred, if needed:
    yminField = -70.0
    ymaxField = 3000.0
    xminField = 0.0 # imaging field
    xmaxField = 3500.0
    
    inFoV = np.where( (xp>xminField) & (xp<=xmaxField) &  (yp>yminField) & (yp<=ymaxField))[0]
    obj = obj[inFoV] ; dat=dat[inFoV]
    
    
    
    
#    stdat = fits.getdata('%sPubCats/UDS_DR8/stars.fits'%rootdir)
#    stdat=stdat[inFoV]
#    okgal = np.where(stdat['star']==0)[0] 


    from match import match
    stID=np.loadtxt('stars/%s_AQ_handchecked_v1.0.cat'%name)
    okst,ok2 = match(obj.ID,stID)
    fits.writeto(name+'setupCands.fits',obj[okst],clobber=True)
    print '%s setup stars'%len(okst)
    
#    gal = obj[okgal]
    gal=obj # no s/g class!
    #stars = obj[]***
    
    
    fits.writeto(name+'info.fits',fInfo,clobber=True)
    fits.writeto(name+'gals.fits',gal,clobber=True)
    
    
    
    
    dra = (gal.ra - fInfo['grpra'])*np.cos(np.deg2rad(gal.dec))
    ddec = gal.dec - fInfo['grpdec']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['grpz'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
    #    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
    #    del gal
    #    gal = np.copy(tgal)
    #    del tgal
    #adisMpc = disMpc
    gal.disMpc = disMpc # note: not really appended to recarray!
    
    
    
    
    #plotClus()
    # initial plot to identify location of cluster in ra,dec and CMD    
    #def plotClus(gal,fInfo):
    col = gal.zmag - gal.irac1 ; mag = gal.irac1 
    #pri = gal.prob
#    xp = gal.xpHam ; yp = gal.ypHam
    xp = gal.xp ; yp = gal.yp
    
    plt.figure()
    plt.subplot(121)
    plt.plot(mag,col,'k,')
    if (gal.izph[0]):
        phzWt = getZphWts(gal,fInfo)
    #        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
    ###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05)  & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim))[0]
    ###        plt.plot( mag[ok],col[ok], 'or', alpha=1, label='|zph-grpz|<=0.05')
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3, label=r'|zph-grpz|<=2$\sigma$')
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=1.0) )[0]
        plt.plot( mag[ok],col[ok], 'or',alpha=0.1,label='zph= 0.7--2.0')
    else:
        ok=np.where( (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3)
        ok=np.where( (gal.disMpc<=0.3) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'ok', alpha=0.5)
        ok=np.where( (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.1)
        
    plt.xlabel('[3.6]')
    plt.ylabel('z - [3.6]')
#    plt.xlim([19,23])
    plt.xlim([17.5,23])
    plt.ylim([-2,5])
    ok=np.where( np.abs(gal.zs-fInfo['grpz'])<=0.01 )[0]
    if(len(ok)>0):
        plt.plot( mag[ok],col[ok], 'ob', alpha=0.7,label='zs')

    
    xxx = np.arange(0,30,0.01)
    plt.plot(xxx,zFnt[1]-xxx,'b--')#,label='faint')
    plt.plot(xxx,zBri[1]-xxx,'b:')#,label='bright')
    plt.plot(xxx,zBri[0]-xxx,'b:')
    plt.plot(xxx,np.polyval(blueCoeffs,xxx),'b-')
    
    plt.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r-',lw=2,label='RS',alpha=0.5)
    plt.plot(xxx,np.polyval(redCoeffs,xxx),'r--',label='RS+0.2')
    #    plt.plot(xxx,np.polyval(redLimCoeffs,xxx),'r-',label='RS+0.5')
    plt.legend(numpoints=1,loc='lower left')
    
    # absolute selection box:
    plt.axvline(irac1Lim,color='m')

#    oki=np.where( (xxx<irac1Lim))[0]
    # intercept between zBri and irac blue cut is at:
    # z-irac1 = 
    yblue = np.polyval(blueCoeffs,xxx)
    
    oki=np.where( (xxx<irac1Lim) & ( yblue>(zBri[0]-xxx) ) )[0]
    plt.plot(xxx[oki],np.polyval(blueCoeffs,xxx[oki]),'k-',lw=3)
    xr1,yr1 = xxx,np.polyval(redCoeffs,xxx)
    xr2,yr2 = xxx,zFnt[1]-xxx
    dyr = yr1-yr2
#    ok2=np.where( (dyr>0) & (xxx<irac1Lim) )[0]
    ok2=np.where( (dyr>0) & (xxx<irac1Lim) )[0]
    #ok1=np.where(dyr<=0)[0]
    ok1=np.where( (xr1<xr2[ok2[0]])  & (yr1>zBri[0]-xxx) )[0]
    plt.plot(xr1[ok1],yr1[ok1],'k-',lw=3)
    plt.plot(xr2[ok2],yr2[ok2],'k-',lw=3)
    
    plt.plot([irac1Lim,irac1Lim],[yr2[ok2[-1]],np.polyval(blueCoeffs,xxx[oki][-1])],'k-',lw=3)    
    
    okB = np.where( (xxx>xr1[ok1[0]]) & ( yblue<zBri[0]-xxx ) )[0]
#    okB = np.where( (xxx>xr1[ok1[0]])  )[0]
    plt.plot(xxx[okB],zBri[0]-xxx[okB],'k-',lw=3)
#    print xxx[okB]
#    print zBri[0]-xxx[okB]
#    ok=np.where()
    plt.text(21.5,2.25,'Fnt',rotation=-45,verticalalignment='top')
    plt.text(20.5,2.25,'Bri',rotation=0,verticalalignment='top')


    plt.title('%s\nz=%.3f  RSintcpt=%.2f'%(name,fInfo['grpz'],rintcpt))



    
    # spatial:
    plt.subplot(122)
    plt.plot(gal.ra,gal.dec,'k,')
    
    plt.plot(fInfo['grpra'],fInfo['grpdec'],'og',ms=20,alpha=0.3,label='clus. cen')
    if (gal.izph[0]):
        phzWt = getZphWts(gal,fInfo)
    #        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
    ###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
    ###        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=1)
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=5.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.2)
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=5.5) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or',alpha=0.1)
    else:  
        ok=np.where( (gal.disMpc<=5.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.2)
        ok=np.where( (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.3)
        ok=np.where( (gal.disMpc<=5.5)& (gal.zmag>zBri[0]) & (gal.zmag<=zBri[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'ok', alpha=0.3,label='Bright')
    
    ok=np.where( np.abs(gal.zs-fInfo['grpz'])<=0.03 )[0]
    if(len(ok)>0):
        plt.plot( gal.ra[ok],gal.dec[ok], 'ob', alpha=0.7,label='zs')
    
    if(len(okst)>0):
        plt.plot(obj.ra[okst],obj.dec[okst],'*g',label='setup stars')
    
    
    plt.xlabel('ra')
    plt.ylabel('dec')
    plt.legend(numpoints=1,loc='lower left')
    

    
    plt.show()
    plt.savefig('%s_diag.png'%name)
    
    """
    plt.figure()
    plt.plot(gal.xpHam,gal.ypHam,'k,')
    ok=np.where( (gal.disMpc<=1.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
    plt.plot(gal.xpHam[ok],gal.ypHam[ok],'ro',alpha=0.5)
    plt.show()
    """









