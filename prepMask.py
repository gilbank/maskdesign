#
# Read in photometric catalogue and set up weighting scheme ready for maskdes
# Make checking plots (spatial, CMD,..)
#
import os,sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from astropy.io import fits
from astropy.io.fits import Column
#from astropy.cosmology import arcsec_per_kpc_proper as asperkpc
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

from numpy.lib import recfunctions as recf
from glob import glob

from config import *

##from astrometry.libkd import spherematch as spm

import maskdesHam as MD2




isilent=1
nMasks=5
#name='SpARCS_0035'
#name='SPT-0205'
#name='CDFS-41'
name='SPT-0546'

#nMasks=3
#name='COS-221'
#name='SXDF64'
#name='SXDF76'
#name='SXDF87'


# //////////////////////////////////////////////////////////////////////
# ---- Parameters for mask design setup
#nMasks=5 # number of masks in set for this cluster
# weighting scheme for each mask in sequence. Probably want different scheme for bright mask at start, followed by same for all subsequent
#allocateSchemes=np.repeat('bright2         ',nMasks)
#allocateSchemes[0]='faint'
#allocateSchemes[0]='bright   '
#allocateSchemes[0]='brightcore'

#allocateSchemes = ['first', 'fillfaint', 'rptfnt', 'rptfnt', 'rptfnt']


xc,yc = ccd_cx,ccd_cy

# //////////////////////////////////////////////////////////////////////


import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage




def match(a,b,badval=-1):
    # much faster matching using dictionary!
    good=np.where(b <> badval)[0]
    d=dict(zip(b[good], np.arange(len(b))[good]  ))
    okd = [d.get(x) for x in a]
    # result will be none if there is no match. need to deal
    okddd=np.array(okd)
    ok=np.array(okddd.nonzero()).astype('int64')
    
    okb=(okddd[ok]).astype('int64')
    oka=ok
        
#    return np.array(oka),np.array(okb)
    return np.reshape(np.array(oka),-1),np.reshape(np.array(okb),-1)


def inBin(x,bin):
    ok=np.where( (x>bin[0]) & (x<=bin[1]) )[0]
    return ok

def getZphWts(gal,fInfo):
        phzWt = np.zeros(len(gal))
        # -- 2nd highest prioirty photoz (within: 0.7--2.0):
        ok = np.where( (gal.zph>0.7) & (gal.zph<2.0) )[0]
        #sel2 = np.where( (dat.zPDF>0.7) & (dat.zPDF<2.0) )[0]
        phzWt[ok] = 0.5
        
        # -- highest priority photo-z (within 2sigma of grpz):
        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph >= fInfo['grpz']) )[0]
#        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph>=grpz), 1.0, 0.0 )[0]
        #   sel = np.where( (dat.zPDF>0) & (2.0*dat.zPDF_l68-dat.zPDF <= grpz) & (2.*dat.zPDF_u68-dat.zPDF>=grpz) & (dat.TYPE==0) )[0]
        phzWt[ok] =1.0
        # *** NOTE: gals with phzs outside this range (or bad photozs) are hence completely rejected! ***
        return phzWt


def genWeights(gal,fInfo,scheme='faint'):
    col = gal.zmag - gal.irac1 ; mag = gal.irac1 ; zmag = gal.zmag
    pri = gal.prob

    print scheme

    # -- redder than CMR:
#    redSeqCoeffs = np.array((fInfo['RSslope'],fInfo['RSintcpt']))
#    redCoeffs = redSeqCoeffs+np.array((0,dRed))
#    redLimCoeffs = redSeqCoeffs+np.array((0,dRedLim))

    yfit = np.polyval(redCoeffs,mag)
#    rej = np.where(col > yfit)[0]


    if (scheme=='first'):
        # set bright high priority
        fntWt = 0.01
        briWt = 0.8
        ok = inBin(zmag,zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,zBri) ; pri[ok]=briWt
        rej = np.where( (col > yfit) | (zmag<zVBri[0]) | (zmag>zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 
#        ok = inBin(zmag,zVBri) ; pri[ok]=0.05
#        ok = np.where( (gal.inMask>0) & (gal.zmag<zBri[1]) )[0]
#        print '%s bright gals in previous mask being ignored'%(len(ok))
#        pri[ok]=0.0

        
    elif (scheme=='fillfaint'):
        
        fntWt = 0.8
        briWt = 0.1
        ok = inBin(zmag,zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,zBri) ; pri[ok]=briWt        
        rej = np.where( (col > yfit) | (zmag<zVBri[0]) | (zmag>zVFnt[1]) )[0]
#        rej = np.where( (col > yfit) & (zmag>zVBri[0]) & (zmag<zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 

        ok = np.where( (gal.inMask>0) & (gal.zmag>=zFnt[0]) )[0]
        print '%s faint gals in previous mask being force-repeated'%(len(ok))
        pri[ok]=1.0    
        ok = np.where( (gal.inMask>0) & (gal.zmag<zBri[1]) )[0]
        print '%s bright gals in previous mask(s) being ignored'%(len(ok))
        pri[ok]=0.0

    
    elif(scheme=='rptfnt'):
        # don't add any new faint objects (just repeat)
        # set weighting high for bright to try to squeeze in
        briWt = 0.8
        fntWt = 0.0
        ok = inBin(zmag,zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,zBri) ; pri[ok]=briWt

        rej = np.where( (col > yfit) | (zmag<zVBri[0]) | (zmag>zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 

        ok = np.where( (gal.inMask>0) & (gal.zmag>=zFnt[0]) )[0]
        print '%s faint gals in previous mask being force-repeated'%(len(ok))
        pri[ok]=1.0
        ok = np.where( (gal.inMask>0) & (gal.zmag<zBri[1]) )[0]
        print '%s bright gals in previous mask being ignored'%(len(ok))
        pri[ok]=0.0

    elif(scheme=='bandbri'):
        # don't add any new faint objects (just repeat)
        # set weighting high for bright to try to squeeze in
        briWt = 0.8
        fntWt = 0.0
        ok = inBin(zmag,zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,zBri) ; pri[ok]=briWt

        rej = np.where( (col > yfit) | (zmag<zVBri[0]) | (zmag>zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 

        ok = np.where( (gal.inMask>0) & (gal.zmag<zBri[1]) )[0]
        print '%s bright gals in previous mask being ignored'%(len(ok))
        pri[ok]=0.0

        
    else:
        print 'scheme %s not found'%scheme
        print flibble # to crash
    
        rej = np.where( (col > yfit) & (zmag>zVBri[0]) & (zmag<zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 
    
    
###    ok = inBin(zmag,zVFnt) ; pri[ok]=0.#1
###    ok = inBin(zmag,zVBri) ; pri[ok]=0.01#5
    
    # -- photoz weighting
    if (gal.izph[0]): # hmm, it seems izph is an array of len(gal), but all elements should be either 1 or 0
        print 'Using photo-z weights'
        phzWt = getZphWts(gal,fInfo)
        """
        phzWt = np.zeros(len(gal))
        # -- 2nd highest prioirty photoz (within: 0.7--2.0):
        ok = np.where( (gal.zph>0.7) & (gal.zph<2.0) )[0]
        #sel2 = np.where( (dat.zPDF>0.7) & (dat.zPDF<2.0) )[0]
        phzWt[ok] = 0.5
        
        # -- highest priority photo-z (within 2sigma of grpz):
        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph >= fInfo['grpz']) )[0]
#        ok = np.where( (2.0*gal.zphLo-gal.zph <= fInfo['grpz']) & (2.0*gal.zphHi-gal.zph>=grpz), 1.0, 0.0 )[0]
        #   sel = np.where( (dat.zPDF>0) & (2.0*dat.zPDF_l68-dat.zPDF <= grpz) & (2.*dat.zPDF_u68-dat.zPDF>=grpz) & (dat.TYPE==0) )[0]
        phzWt[ok] =1.0

        # *** NOTE: gals with phzs outside this range (or bad photozs) are hence completely rejected! ***
        """
        pri = pri * phzWt
    # --
  
    # -- redder than CMR:
    yfit = np.polyval(redCoeffs,mag)
#    rej = np.where(col > yfit)[0]
    rej = np.where( (col > yfit) & (zmag>zVBri[0]) & (zmag<zVFnt[1]) )[0]
    pri[rej] = 0.0###1#5 

    yfit = np.polyval(redLimCoeffs,mag)
#    rej = np.where(col > yfit)[0]
    rej = np.where( (col > yfit) & (zmag>zVBri[0]) & (zmag<zVFnt[1]) )[0]
    pri[rej] = 0.00 
    
    # --
    
    
    # -- blue exclusion cut:
    # (careful, this is in terms of irac1 mag, not z, as above)
    yfit = np.polyval(blueCoeffs,mag)
    rej = np.where(col < yfit)[0]
    pri[rej]=0.0
    # --
    
    # -- irac1 exclusion cut:
    rej = np.where(mag>irac1VFnt)[0]
    pri[rej]=0.0
    bad = np.where(mag>irac1Lim)[0]
    pri[bad]=pri[bad]*0.#1 # downweight v faintest bin

    
    bad = np.where(mag<0.0)[0]
    pri[bad]=0.0 # IRAC undetected?***
    # --

    
    radwt =  (1. - 0.4*gal.disMpc)**2 #*** probably not extreme enough
    
    
    # update weights:
    gal.prob = pri
    
    if (iRadialWt):
        notMustHaves = np.where(pri<1.0)[0]
        gal.prob[notMustHaves] = pri[notMustHaves]* radwt[notMustHaves]





def plotCMD(gal,fInfo,axname):
    col = gal.zmag - gal.irac1 ; mag = gal.irac1 
    pri = gal.prob
    axname.plot(mag,col,'k,')
    axname.set_xlabel('[3.6]')
    axname.set_ylabel('z - [3.6]')
    axname.set_xlim([19,23])
    axname.set_ylim([-2,6])
    
    xxx = np.arange(0,30)
    axname.plot(xxx,zFnt[1]-xxx,'b--',label='faint')
    axname.plot(xxx,zBri[1]-xxx,'b:',label='faint')
    axname.plot(xxx,np.polyval(blueCoeffs,xxx),'b-')
    

    axname.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r:')
    axname.plot(xxx,np.polyval(redCoeffs,xxx),'r-')
    axname.plot(xxx,np.polyval(redLimCoeffs,xxx),'r--')
    
    # wts:
    sz=pri*100 +0.1
    axname.scatter(mag,col,s=sz,alpha=0.5,color='b')
    

# initial plot to identify location of cluster in ra,dec and CMD    
def plotClus(gal,fInfo):
    col = gal.zmag - gal.irac1 ; mag = gal.irac1 
    pri = gal.prob
    xp = gal.xp ; yp = gal.yp
    
    plt.figure()
    plt.subplot(121)
    plt.plot(mag,col,'k,')
    if (gal.izph[0]):
        phzWt = getZphWts(gal,fInfo)
#        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05)  & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim))[0]
###        plt.plot( mag[ok],col[ok], 'or', alpha=1, label='|zph-grpz|<=0.05')
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3, label=r'|zph-grpz|<=2$\sigma$')
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=1.0) )[0]
        plt.plot( mag[ok],col[ok], 'or',alpha=0.1,label='zph= 0.7--2.0')
    else:
        ok=np.where( (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3)
        ok=np.where( (gal.disMpc<=0.3) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'ok', alpha=0.5, label='r<0.3Mpc')
        ok=np.where( (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.1)
        
    plt.xlabel('[3.6]')
    plt.ylabel('z - [3.6]')
    plt.xlim([19,23])
    plt.ylim([-2,6])
    plt.legend(numpoints=1)
    
    xxx = np.arange(0,30)
    plt.plot(xxx,zFnt[1]-xxx,'b--',label='faint')
    plt.plot(xxx,zBri[1]-xxx,'b:',label='bright')
    plt.plot(xxx,np.polyval(blueCoeffs,xxx),'b-')
    
    plt.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r:',label='RS')
    plt.plot(xxx,np.polyval(redCoeffs,xxx),'r--',label='RS+%.2f'%dRed)
    plt.plot(xxx,np.polyval(redLimCoeffs,xxx),'r-',label='RS+%.2f'%dRedLim)
    plt.legend(numpoints=1)
    
    # spatial:
    plt.subplot(122)
    plt.plot(gal.ra,gal.dec,'k,')

    plt.plot(fInfo['grpra'],fInfo['grpdec'],'og',ms=20,alpha=0.3)
    if (gal.izph[0]):
        phzWt = getZphWts(gal,fInfo)
#        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
###        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=1)
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=5.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.2)
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=5.5) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or',alpha=0.1)
    else:  
        ok=np.where( (gal.disMpc<=5.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.2)
        
 
    plt.xlabel('ra')
    plt.ylabel('dec')
    
    
    plt.show()
    
    
# in pixels
def drawMpcCircle(xc,yc,nMpc,fInfo,axname):
    radius = (nMpc*1000.0*asperkpc(fInfo['grpz'])).value # arcsec
    radius = radius/pxscale # pix
    circ = Circle((xc,yc),radius, fill=False, facecolor='none', edgecolor='r')
    axname.add_patch(circ)

def calcdisMpc(fInfo,gal):
    # add distance of every gal from rabcg, decbcg in Mpc
    
    
    dra = (gal.ra - fInfo['grpra'])*np.cos(np.deg2rad(gal.dec))
    ddec = gal.dec - fInfo['grpdec']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['grpz'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
#    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
#    del gal
#    gal = np.copy(tgal)
#    del tgal
    adisMpc = disMpc
    
    if(0): # use spm instead
        from astrometry.libkd import spherematch as spm
        oka,okb,disdeg = spm.match_radec(gal.ra,gal.dec,fInfo['grpra'],fInfo['grpdec'],1.0,nearest=False)
        adisMpc = np.ones(len(gal))
        disMpc = disdeg*3600.0 / convfac /1000.0
        adisMpc[oka] = disMpc # needed with spm
    

    return adisMpc

def plotSpatial(gal,fInfo,axname):
    xp = gal.xp ; yp = gal.yp 
    pri = gal.prob
    axname.plot(xp,yp,'k,')
    axname.set_xlabel('xp')
    axname.set_ylabel('yp')
#    axname.set_xlim([19,23])
#    axname.set_ylim([-2,6])

    # wts:
    sz=pri*100 +0.1
    axname.scatter(xp,yp,s=sz,alpha=0.5,color='b')

#    MD.drawGMOS()
    MD2.drawHam(axname)
    
    # draw 0.5 Mpc radius circle
    ##xc,yc = MD.MaskpixfromSky(fInfo['cra'],fInfo['cdec'],fInfo['rotang'],fInfo['grpra'],fInfo['grpdec'])
    drawMpcCircle(xc,yc,0.5,fInfo,axname)
    drawMpcCircle(xc,yc,1.0,fInfo,axname)
    drawMpcCircle(xc,yc,2.0,fInfo,axname)

def writeInput(inname,gal,fInfo,sLen=nomsLen,sWid=nomsWid,sTilt=nomsTilt,stars=None):
    # write the input file for desmask()
    #ID,xobj,yobj,ra,dec,pri,slen,swid,stilt = np.loadtxt('tmpCDFS41.dat',unpack=True)


    use=np.where(gal.prob>0.0)[0]
#    np.savetxt(inname,np.transpose( (gal.ID,gal.xp,gal.yp,gal.ra,gal.dec,gal.prob,\
#                                             np.repeat(sLen,len(gal)), np.repeat(sWid,len(gal)), np.repeat(sTilt,len(gal)) ) ) )

    f = open(inname,'w')
    np.savetxt(f,np.transpose( (gal.ID[use],gal.xp[use],gal.yp[use],gal.ra[use],gal.dec[use],gal.prob[use],\
                                             np.repeat(sLen,len(use)), np.repeat(sWid,len(use)), np.repeat(sTilt,len(use)) ) ),\
                                             fmt='%s %.2f %.2f %.7f %.7f %.4f %.2f %.2f %.3f' )
    try: #(len(stars))
        use=np.arange(len(stars)) ; stars.prob = np.repeat(9.0,len(stars))
        try:
            uid = np.loadtxt(name+'.stars')
            ok1,ok2 = match(uid,stars.ID)
            stars = stars[ok2]
            use=np.arange(len(stars)) ; stars.prob = np.repeat(9.0,len(stars))
        except:
            pass # no .stars file
        np.savetxt(f,np.transpose( (stars.ID[use],stars.xp[use],stars.yp[use],stars.ra[use],stars.dec[use],stars.prob[use],\
                                              np.repeat(starsLen,len(use)), np.repeat(starsWid,len(use)), np.repeat(0.0,len(use)) ) ),\
                                              fmt='%s %.2f %.2f %.7f %.7f %.4f %.2f %.2f %.3f' )
    except:
        pass
    f.close()
    
    
def tagMask(gal,fInfo,mID,masknum=1):    
    # add inMask bitflag:
    ok1,ok2 = match(gal.ID,mID)
#    mag = zmag[ok1]
    mgal = gal[ok1]
    mag = mgal.zmag
    # update inMask flag with binary to indicate which number(s) mask in
    gal.inMask[ok1] = gal.inMask[ok1] + 2**(masknum-1)


def evalCat(gal,fInfo):
    col = gal.zmag - gal.irac1
    
    # Find number of objects in appropriate mag,distance, etc. bins in whole catalogue (with appropriate cuts)
    # for completeness calculations:
    inner = (gal.disMpc<=0.5)
    outer = ( (gal.disMpc>0.5) & (gal.disMpc<=1.0) )
    outsk = ( (gal.disMpc>1.0) )
    
    bri = ( (gal.zmag > zBri[0]) & (gal.zmag <= zBri[1]) )
    fnt = ( (gal.zmag > zFnt[0]) & (gal.zmag <= zFnt[1]) )
    
    # -- redder than CMR:
#    yfitRedLim = np.polyval(redLimCoeffs,gal.irac1)
    yfitRedLim = np.polyval(redCoeffs,gal.irac1)

    # -- blue exclusion cut:
    # (careful, this is in terms of irac1 mag, not z, as above)
    yfitBlueLim = np.polyval(blueCoeffs,gal.irac1)

    if (gal.izph[0]):
        zphWts = getZphWts(gal,fInfo)

    # othCuts must be applied to each condition!
    if (gal.izph[0]):
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal.irac1<irac1VFnt) \
                    & (gal.irac1>0.0) \
                    & (zphWts>0.4) )
    else:
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal.irac1<irac1VFnt) \
                    & (gal.irac1>0.0) \
                    )


    hasSlit = (gal.inMask>0)

    innBri = np.sum( inner & bri & othCuts )
    outerBri = np.sum( outer & bri & othCuts )
    outskBri = np.sum( outsk & bri & othCuts )
    innFnt = np.sum( inner & fnt & othCuts )
    outerFnt = np.sum( outer & fnt & othCuts )
    outskFnt = np.sum( outsk & fnt & othCuts )
#    print 'len(innBri)=%s'%len(innBri)
    #print innBri,outerBri,innFnt,outerFnt,outskFnt
    innBriS = np.sum( inner & bri & othCuts & hasSlit)
    outerBriS = np.sum( outer & bri & othCuts & hasSlit)
    outskBriS = np.sum( outsk & bri & othCuts & hasSlit)
    innFntS = np.sum( inner & fnt & othCuts & hasSlit)
    outerFntS = np.sum( outer & fnt & othCuts & hasSlit)
    outskFntS = np.sum( outsk & fnt & othCuts & hasSlit)
    #print innBriS,outerBriS,innFntS,outerFntS,outskFntS

    print
    print 'Bright:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innBriS,innBri, outerBriS,outerBri, outskBriS,outskBri )
    print 'Faint:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innFntS,innFnt, outerFntS,outerFnt, outskFntS,outskFnt )

    return np.array((innBriS, outerBriS, outskBriS)),\
            np.array((innBri, outerBri, outskBri)),\
            np.array((innFntS, outerFntS, outskFntS)),\
            np.array((innFnt, outerFnt, outskFnt)),\


def evalMask(gal,fInfo,masknum=0,nTotMasks=3):
#    # evaluate mask:

    # if masknum==0, evaluate all objects inMask, otherwise only specified mask number:
    if (masknum):
#        reqBit = 2**(masknum-1)
        bitFlag = [np.binary_repr(y,nTotMasks) for y in gal.inMask.astype('int')]        
#        ok1 = np.where(gal.inMask==reqBit)[0]
        ok1 = [j for j,y in enumerate(bitFlag) if y[nTotMasks-masknum]=='1' ]
        #ok1 = [j for j,y in enumerate(maskFlag) if y[2]=='1']
    else:
        ok1 = np.where(gal.inMask>0)[0]
    
#    mag = zmag[ok1]
    mgal = gal[ok1]
    mag = mgal.zmag
    # as fn of zmag:
#    print '%s fnt; %s bri; %s vFnt; %s vBri'\
#        %( len(inBin(mag,zFnt)),len(inBin(mag,zBri)),len(inBin(mag,zVFnt)),len(inBin(mag,zVBri)) )
    # as fn of distance:

    ###oka,okb,dis = spm.match_radec(mgal.ra,mgal.dec,fInfo['cra'],fInfo['cdec'],1.0,nearest=False)

    dra = (gal.ra - fInfo['grpra'])/np.cos(np.deg2rad(gal.dec))
    ddec = gal.dec - fInfo['grpdec']
    dis = np.sqrt(dra**2 + ddec**2)
    
    disMpc = dis*3600.0 / (1.0*1000.0*asperkpc(fInfo['grpz'])).value
    inn = np.where( disMpc <= 0.5)[0]
#    print dis
#    print disMpc
#    print '%s within 0.5 Mpc'%len(inn)
#    print

    # update inMask flag with binary to indicate which number(s) mask in
#    gal.inMask[ok1] = gal.inMask[ok1] + 2**(masknum-1)


def bitMask(x):
    # turn N array of floats (up to 2**(M-1))into
    # into N*M bitmask array 
    strBits = [binary_repr(y,3) for y in x.astype('int')]
    

def writeGMMPS(id,ra,dec,x_ccd,y_ccd,mag,priority):
    #**** NOT WORKING YET ******
    # let's just try a FITS table of the type that's supposed to work:
    """
    The Object Table is a FITS table that contains the following mandatory columns:
    ID    Unique object id (integer)
    RA    RA in hours (real)
    DEC    Dec in degrees (real)
    x_ccd    X coordinate of object position in pixels (real)
    y_ccd    Y coordinate of object position in pixels (real)
    MAG    (Relative) magnitude of object (real)
    priority    Priority of object (char*1; "0/1/2/3/X")
    The order of the columns is important. The column names are case sensitive.
    """
    
    
    nn=len(id)
    out = np.array( [id,ra,dec,x_ccd,y_ccd,mag,priority], \
                      dtype=[('ID',np.int16), ('RA',np.float32), ('DEC',np.float32),('x_ccd',np.float32),\
                             ('y_ccd',np.float32), ('MAG', np.float32), ('priority',np.int16)] )
    
    c1 = Column(name='ID', format='4A', array=id)
    c2 = Column(name='RA', format='E', array=ra)
    c3 = Column(name='DEC', format='E', array=dec)
    c4 = Column(name='x_ccd', format='E', array=x_ccd)
    c5 = Column(name='y_ccd', format='E', array=y_ccd)
    c6 = Column(name='MAG', format='10A', array=mag)
    c7 = Column(name='priority', format='10A', array=priority)

#    coldefs = fits.ColDefs([c1, c2, c3, c4, c5, c6, c7])
#    tbhdu = fits.BinTableHDU.from_columns(coldefs)
#    tbhdu.writeto('test.fits')
    fits.writeto('testODF1.fits',out,clobber=True)


def verifyDes(maskname): # maskname should really be, e.g. bestSpARCS_0035in1.txt, to check the best design of each
    id,xp,yp,ra,dec,wt,slen,swid,pri = np.loadtxt(maskname,unpack=True)
    plt.figure()
    plt.plot(xp,yp,'k,')
    sz=wt*100.+0.1
    plt.scatter(xp,yp,s=sz,color='r',alpha=0.3)

    outname=maskname.replace('in','out')
#    outname=maskname.replace('des','out')
    sra,sdec,sdec,sid = np.loadtxt(outname,unpack=True)

    ok1,ok2=match(id,sid)
    plt.scatter(xp[ok1],yp[ok1],s=sz[ok1],color='b',alpha=0.3)
    plt.show()



def updateinMask(gal,maskfile):
    mra,mdec,mpri,mID = np.loadtxt(maskfile, unpack=True)
    ok1,ok2=match(gal.ID,mID)
    gal.inMask[ok1]=1.0










#def prepMask(name,nMasks=5,isilent=True):
if(1):    
    
    #gal = fits.getdata('%sgals.fits'%name)
    # store array of inMask flags for each iteration, so we can reconstruct best mask:
    #ainMask = np.zeros((len(gal),nMultiIter))
    
    # Clean up old results files:
    os.system('rm -f %s*in*txt'%name)
    os.system('rm -f %s*out*txt'%name)
    os.system('rm -f best%s*txt'%name)
    os.system('rm -f %s*reg'%name)
    
    
    iplot=1
    bestScore = 0 # (let's do this old-fashioned way and ignore bitFlags)
    for kk in range(nMultiIter): # run this many iterations of the full set of masks
        print 'ITER(multi) =%s'%kk
    
        # force silent:
        if (isilent):
            sys.stdout = open(os.devnull, "w") # suppress printing for each mask
    
        fInfo = fits.getdata('%sinfo.fits'%name) ; fInfo=fInfo[0] # convert to scalars
        gal = fits.getdata('%sgals.fits'%name)
        try:
            stars = fits.getdata('%ssetupCands.fits'%name)
#            if(glob('%s.star*'%name)):

            if(len(stars)>4):
                # take two highest and lowest in y:
                sss=np.argsort(stars.yp)
                suse = np.hstack((sss[0:2],sss[-2::]))
                stars = stars[suse]

#                stars=stars[0:4] #*** kludge
#                useStars = np.loadtxt('%s.stars'%name)
#                sok1,sok2 = match(stars.ID,useStars)
#                stars = stars[sok1]
        except:
            stars = None
            
        redSeqCoeffs = np.array((fInfo['RSslope'],fInfo['RSintcpt']))
        redCoeffs = redSeqCoeffs+np.array((0,dRed))
        redLimCoeffs = redSeqCoeffs+np.array((0,dRedLim))
    
        
    
        if (NSFlag):
            print "applying shuffle offset (%.2f pixels) to galaxy positions"%shuffleOffPix
            gal.yp = gal.yp+shuffleOffPix
            
    
        print
        print '---------- Field info -----------'
        print 'Mask centre     ra,dec = %.6f, %.6f; rotang = %.2f'%(fInfo['cra'],fInfo['cdec'],fInfo['rotang'])
        if (fInfo['grpra']<0.0):
            print '*** using field centre as clus centre ***'
            fInfo['grpra'] = fInfo['cra'] ; fInfo['grpdec'] = fInfo['cdec']
        print 'Cluster centre: ra,dec = %.6f, %.6f; redshift = %.4f'%(fInfo['grpra'],fInfo['grpdec'],fInfo['grpz'])
        print '---------------------------------'
        print
        
        # CAREFUL: this must only be reset at the start of 
        inMask = np.zeros(len(gal)) # flag whether gal has been assigned to a mask or not:
        prob = np.zeros(len(gal)) # probability of being included in mask
        
        
        # calculate distances:
        disMpc = calcdisMpc(fInfo,gal)
        
        # add all these other arrays to gal array, to make sure everything stays in sync:
        gal = recf.append_fields(gal,('inMask','prob','disMpc'),(inMask,prob,disMpc),asrecarray=True,usemask=False)
        # trim to field:
        inFoV = np.where( (gal.xp>xminField) & (gal.xp<=xmaxField) &  (gal.yp>yminField) & (gal.yp<=ymaxField))[0]
        gal = gal[inFoV]
        
        print '%s gals in full catalogue within imaging field'%(len(gal))
        print
        
#        if (kk==0):
#            plotClus(gal,fInfo)
    
        
        for jj in range(nMasks):
        
            print 'Mask %s:'%(jj+1) 
            print   
            # Mask 1 - allocate bright targets first:
            #genWeights(gal,fInfo, scheme='bright')
            genWeights(gal,fInfo, scheme=allocateSchemes[jj])
            
            
            if(allocateSchemes[jj][0:4]=='band'):
                NSmode='band'
            else:
                NSmode='micro'
    
            # run design:
            inname = name+'in%s.txt'%(jj+1)
            writeInput(inname,gal,fInfo,stars=stars)
            MD2.desmask( fInfo['cra'],fInfo['cdec'],fInfo['rotang'],inname,niter=nSingleIter ,iplot=plotFlag, sortScheme=sortScheme, NSmode=NSmode )
            
            # evaluate design:
            maskname = inname.replace('in','out')
            mra,mdec,mpri,mID = np.loadtxt(maskname, unpack=True)
            #ok1,ok2 = match(gal.ID,mID)
            tagMask(gal,fInfo,mID,masknum=(jj+1))
    #        evalMask(gal,fInfo,masknum=(jj+1),nTotMasks=nMasks)
            ok1 = np.where(gal.inMask==1)[0]
            
            updateinMask(gal,maskname)
            
            print '%s: %s slits; total weight=%.2f'%(maskname,len(mra),np.sum(mpri))
            print
            
            os.system('cp -f ds9.reg %s'%(maskname.replace('.txt','.reg')))
            
            (nBriS,nBri, nFntS, nFnt) = evalCat(gal,fInfo)       
        #    writeGMMPS(gal.ID[ok1],gal.ra[ok1],gal.dec[ok1],gal.xp[ok1],gal.yp[ok1],gal.zmag[ok1],np.ones(len(ok1)))
        
        # evaluate this set of masks:    
        (nBriS,nBri, nFntS, nFnt) = evalCat(gal,fInfo)
    
    ###    Score = np.copy(np.sum(nFntS[0:2])+0.1*(np.sum(nBriS[0:2])))
        if (np.sum(nFntS[0:2])<11):
    #        Score=0.0
            Score = np.copy(0.5*np.sum(nFntS[0:2])+0.05*(np.sum(nBriS[0:2])))
        else:
            Score = np.copy(0.2*np.sum(nFntS[0:2])+1.0*(np.sum(nBriS[0:2])))
    
        if (Score > bestScore):
            bestScore = np.copy(Score)
            for i in range(nMasks):
                inname = name+'in%s.txt'%(i+1)
                os.system('cp -f %s best%s'%(inname,inname))
                maskname = inname.replace('in','out')
                os.system('cp -f %s best%s'%(maskname,maskname))
                regname = maskname.replace('.txt','.reg')
                os.system('cp -f %s best%s'%(regname,regname))
                gmmpsname = inname.replace('in','gmmps')
                os.system('cp -f %s best%s'%(gmmpsname,gmmpsname))

    
    #    ainMask[:,kk] = np.copy(gal.inMask)
        """
        if(kk==0):
            ainMask = np.copy(gal.inMask).astype(np.int)
            aScore = np.copy(np.sum(nFntS[0:2])) # maximise number of faint targets within 1Mpc
    
            # backup all files:
            for i in range(nMasks):
                inname = name+'in%s.txt'%(i+1)
                os.system('cp -f %s best%s'%(inname,inname))
                maskname = inname.replace('in','out')
                os.system('cp -f %s best%s'%(maskname,maskname))
                os.system('cp -f ds9.reg %s'%(maskname.replace('.txt','.reg')))
        else:
            ainMask = np.vstack((ainMask,np.copy(gal.inMask).astype(np.int)))
            #aScore = np.vstack((np.copy(aScore),np.copy(np.sum(nFntS[0:1]))))
            aScore=np.append(aScore,np.sum(nFntS[0:2]))
            # backup all files:
            for i in range(nMasks):
                inname = name+'in%s.txt'%(i+1)
                os.system('cp -f %s best%s'%(inname,inname))
                maskname = inname.replace('in','out')
                os.system('cp -f %s best%s'%(maskname,maskname))
                regname = maskname.replace('.txt','.reg')
                os.system('cp -f %s best%s'%(regname,regname))
    
        print '*** score= %.0f'%np.sum(nFntS[0:2])#,aScore,ainMask
        
        print '%s unique objects in all masks of this set'%len(gal.inMask.nonzero()[0])
        """
        
        
        
        # un-mute:
        sys.stdout = sys.__stdout__
        
    
    #if (nMultiIter>1):
    #    best =  np.argmax(aScore)
    #    gal.inMask = ainMask[best]
    
    """
    print
    print "Best Mask Set:"
    # Ah, there's a chance the best set stored in the bitFlag array and those in the output files may be different, 
    # but will have the same score! e.g., 7,4 vs 6,5 in inner two bins:
    f00 = evalCat(gal,fInfo)
    print bestScore
    print '\n\n\n'
    """
    
    # read in masks to check:
    for i in range(nMasks):
        maskname = 'best%sout%s.txt'%(name,i+1)
        mra,mdec,mpri,mid = np.loadtxt(maskname,unpack=True)
        gal.inMask[:]=0.0
        ok1,ok2 = match(gal.ID,mid)
        gal.inMask[ok1]=1.0
        
        print
        print '%s:'%maskname
        (nBriS,nBri, nFntS, nFnt) = evalCat(gal,fInfo)
    
    # run again without setting to zero in between to evaluate total set:
    
    print
    print
    print "Best Mask Set (all %s masks, not just the filename below!):"%nMasks
    for i in range(nMasks):
        maskname = 'best%sout%s.txt'%(name,i+1)
        mra,mdec,mpri,mid = np.loadtxt(maskname,unpack=True)
    #    gal.inMask[:]=0.0
        ok1,ok2 = match(gal.ID,mid)
        gal.inMask[ok1]=1.0
        
        print
        print '%s:'%maskname
        if(i<nMasks-1): 
            sys.stdout = open(os.devnull, "w") # suppress printing for each mask
        else:
            sys.stdout = sys.__stdout__
        (nBriS,nBri, nFntS, nFnt) = evalCat(gal,fInfo)
    
        
    
    
    
    
    """
    # -- Recover masks from best iteration:
    bitFlag = [np.binary_repr(y,nMasks) for y in gal.inMask.astype('int')]        
    #        ok1 = np.where(gal.inMask==reqBit)[0]
    nTotMasks=nMasks
    masknum=1
    for ii in np.arange(nMasks):
        masknum=ii+1
        ogalprob = np.copy(gal.prob)
        ok1 = [j for j,y in enumerate(bitFlag) if y[nTotMasks-masknum]=='1' ]
        gal.prob[:]=0.0
        gal.prob[ok1]=ogalprob[ok1]
        #writeInput(name+'des%s.txt'%masknum,gal,fInfo,sLen=nomsLen,sWid=nomsWid,sTilt=nomsTilt)
        np.savetxt(name+'out%s.txt'%masknum,np.transpose((gal.ra[ok1],gal.dec[ok1],gal.prob[ok1],gal.ID[ok1])))
    """
    
    
    
    
    
    
    """
    Bright:
    r<0.5Mpc: 7/13;  r:0.5--1.0Mpc: 8/21;  r>1.0Mpc: 11/37
    Faint:
    r<0.5Mpc: 2/9;  r:0.5--1.0Mpc: 5/22;  r>1.0Mpc: 14/41
    """
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    print
    print
    print 'NOTE: galaxy positions have been offset in y by %.2f arcsec \nto allow for shuffle buffer. VERIFY NOD DISTANCE AND DIRECTION'%(shuffleOffPix*pxscale)
    print ''
    
    
    
    # Simple checking of masks in best set:
    
    for ii in range(nMasks):
        #bestinfile = 'best%sin%s.txt'%(name,ii+1)
        #bestoutfile = 'best%sout%s.txt'%(name,ii+1)
    
        #id,xp,yp,ra,dec,wt,slen,swid,pri = np.loadtxt(bestinfile,unpack=True)
        #mra,mdec,mpri,mID = np.loadtxt(bestoutfile, unpack=True)

        bestgmmpsfile = 'best%sgmmps%s.txt'%(name,ii+1)
        id,rah,dec,xccd,yccd,mag,pri,slen,swid,stilt = np.loadtxt(bestgmmpsfile,unpack=True)

        # clean up intermediate files to avoid confusion later!
        os.system('rm -f %s*in*txt'%name)
        os.system('rm -f %s*out*txt'%name)
        os.system('rm -f %s*reg'%name)
        os.system('rm -f %s*gmmps*txt'%name)


        
        
"""    
def doall():
    clusnames = ['SpARCS_0035','SPT-0205',]
    for name in clusnames:
        prepMask(name,nMasks=5)
        
    grpnames = ['COS-221','SXDF64','SXDF76','SXDF87']
    for name in grpnames:
        prepMask(name,nMasks=3)
"""    
    
    
    #name='SPT-0205'
#name='SpARCS_0035'
#name='COS-221'
#name='SXDF64'
name='SXDF76'

    
