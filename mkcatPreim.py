from astropy.io import fits,ascii
from astropy import wcs as pywcs

import numpy as np
import matplotlib.pyplot as plt

from numpy.lib import recfunctions as recf
#from maskdes import *
#from astropy.cosmology import arcsec_per_kpc_proper as asperkpc
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper
from astropy.coordinates import SkyCoord
import astropy.units as u


# mkcatPreim('SpARCS_0035','SPARCS0035_zband_IRAC2bands_v1.1_starcheck.cat')
# dumb way to do intersection of 2 lines by hacking:
# http://stackoverflow.com/questions/3252194/numpy-and-line-intersections
def line_intersect(coeffs0, coeffs1):
    m0=None ; m1=None
    x0 = np.array(15.0) ; x1 = np.array(20.0)
    p0 = np.array( (x0,np.polyval(coeffs0,x0)) )
    p1 = np.array( (x0,np.polyval(coeffs1,x0)) )
    q0 = np.array( (x1,np.polyval(coeffs0,x1)) )
    q1 = np.array( (x1,np.polyval(coeffs1,x1)) )
#def line_intersect(p0, p1, m0=None, m1=None, q0=None, q1=None):
    ''' intersect 2 lines given 2 points and (either associated slopes or one extra point)
    Inputs:
        p0 - first point of first line [x,y]
        p1 - fist point of second line [x,y]
        m0 - slope of first line
        m1 - slope of second line
        q0 - second point of first line [x,y]
        q1 - second point of second line [x,y]
    '''
    if m0 is  None:
        if q0 is None:
            raise ValueError('either m0 or q0 is needed')
        dy = q0[1] - p0[1]
        dx = q0[0] - p0[0]
        lhs0 = [-dy, dx]
        rhs0 = p0[1] * dx - dy * p0[0]
    else:
        lhs0 = [-m0, 1]
        rhs0 = p0[1] - m0 * p0[0]

    if m1 is  None:
        if q1 is None:
            raise ValueError('either m1 or q1 is needed')
        dy = q1[1] - p1[1]
        dx = q1[0] - p1[0]
        lhs1 = [-dy, dx]
        rhs1 = p1[1] * dx - dy * p1[0]
    else:
        lhs1 = [-m1, 1]
        rhs1 = p1[1] - m1 * p1[0]

    a = np.array([lhs0, 
                  lhs1])

    b = np.array([rhs0, 
                  rhs1])
    try:
        px = np.linalg.solve(a, b)
    except:
        px = np.array([np.nan, np.nan])

    return px



def mkcatPreim(name,catfile,dcol=0.0):

    #dat = ascii.read('SPT0205_zband_IRAC2bands.cat',data_start=2)
    dat = ascii.read(catfile,data_start=2)
    # http://www.strw.leidenuniv.nl/galaxyevolution/ULTRAVISTA/Ultravista/K-selected.html
    """
    Preliminary catalog for 0035.  Header has column names.  (xx) is aperture size in arcsec (diameter) 
    use 3.6" for colors, and I1tot for total magnitudes.  
    Star/galaxy not done correctly in this, do not use as a parameter.  
    RA and DEC are in the WCS of the preimage, small offset from IRAC+original z-band.
     
    SPARCS0035_zband_IRAC2bands.cat
    """
    
    #hdr = fits.getheader('../../DATA/PreIms/gc0035_coadd.fits',1)
    #wcs = pywcs.WCS(header=hdr)
    #xpHam,ypHam = wcs.wcs_sky2pix(dat['RA'],dat['DEC'],1)
    
    xpHam = dat['xpHam'] ; ypHam = dat['ypHam']
    
    
    from config import *
 
    
    
    # put in master table of clusters later ***
    if(name=='SpARCS_0035'):
        cra,cdec = 8.9571000000000005,-43.206800000000001
        rotang=180.
#        grpra,grpdec=-1,-1
        grpra,grpdec = 8.9571000000000005,-43.206800000000001
        grpz=1.335
    else:
        rootdir='/home/gilbank/Proj/goGreen/'

        mm = fits.getdata('%sMaskDesign/maskinfo.fits'%rootdir)
        ok=np.where(mm.Name==name)[0]
        #name='COS221'
        rotang = mm['PA'][ok]
        ccoo= SkyCoord(mm['cra'][ok],mm['cdec'][ok],unit=(u.hour,u.deg))
        cra,cdec = ccoo.ra.value[0],ccoo.dec.value[0]
        grpcoo= SkyCoord(mm['grpra'][ok],mm['grpdec'][ok],unit=(u.hour,u.deg))
        grpra,grpdec = grpcoo.ra.value[0],ccoo.dec.value[0]
        grpz=mm['grpz'][ok]

#        print ' ra,dec not known'
#        stop()
    
    
    
    # nominal RS fit:
    ##--- by eye
    x0,y0=20.15,2.06 ; x1,y1=21.83,1.86
    rslope=(y1-y0)/(x1-x0)
    rslope=0.0 #***
    rintcpt=y0-rslope*x0 +0.35+0.2+0.2+dcol
    redSeqCoeffs = np.array((rslope,rintcpt))
    redCoeffs = redSeqCoeffs+np.array((0,dRed))

    
   
    xp = xpHam#*sclH - ccdH_cx + ccd_cx
    yp = ypHam#*sclH - ccdH_cy + ccd_cy
    
  
    # Field Info:
    #fInfo = np.recarray(cra, dtype=['cra'])
    t = np.recarray(0,formats=('float'))
    fInfo = recf.append_fields(t,('cra','cdec','rotang','grpra','grpdec','grpz','RSslope','RSintcpt'),\
                               (cra,cdec,rotang,grpra,grpdec,grpz,rslope,rintcpt),\
                               asrecarray=True,usemask=False)
    fInfo = recf.drop_fields(fInfo,'f0',asrecarray=True,usemask=False)
    #fInfo=fInfo[0]
    
    id = dat['ID']
    ra = dat['RA']
    dec = dat['DEC']
    irac1 = dat['I1(3.6)']
    eirac1 = dat['eI1(3.6)']#***
    zmag = dat['zAp(3.6)']
    ezmag = dat['ez(3.6)']#***
    
    
    
    zph = -np.ones(len(dat))
    zphLo = -np.ones(len(dat))
    zphHi = -np.ones(len(dat))
    izph = -np.zeros(len(dat))
    izs = False
    
    zs = -np.ones(len(id))
    
    if(name=='CDFS-41'):
        foo=fits.getdata('cdfs41_members.fits',1)
        """ is Julie's astrometry bad, or am I not using this routine correctly???
        from astropy.coordinates import match_coordinates_sky
#        foo['ra'],foo['dec'],foo['zspec'],foo['zflag']
        ccoo= SkyCoord(ra,dec,unit=(u.hour,u.deg))
        zcoo= SkyCoord(foo['ra'],foo['dec'],unit=(u.hour,u.deg))
        idx,sep2d,dist3d = match_coordinates_sky(zcoo,ccoo, nthneighbor=1 )
        print sep2d
        print ccoo[idx]
        print zcoo
        zs[idx]=foo['zspec']
        """
    #***
    #zsq = -np.ones(len(id)) # zs quality flag ACTUALLY LET'S JUST ASSUME THE MASK DESIGN CODE ONLY GETS GIVEN GOOD ZSs
    
    obj = recf.append_fields(t,('ID','ra','dec','irac1','eirac1','zmag','ezmag','zph',\
                                'zphLo','zphHi','izph','zs','xp','yp'),\
                             (id,ra,dec,irac1,eirac1,zmag,ezmag,zph,zphLo,zphHi,izph,zs,xp,yp),\
                               asrecarray=True,usemask=False)
    
    obj = recf.drop_fields(obj,'f0',asrecarray=True,usemask=False)






    
    # TRIM FIELD
    
    #xp,yp = MaskpixfromSky(fInfo['cra'][0],fInfo['cdec'][0],fInfo['rotang'][0],obj.ra,obj.dec)
    
    """
    yminField = 70.0
    ymaxField = 2247.0
    xminField = 450.0 # imaging field
    xmaxField = 2660.0
    """
    # let's go with wider cuts so the field can be recentred, if needed:
    yminField = -70.0
    ymaxField = 2500.0
    xminField = 0.0 # imaging field
    xmaxField = 3500.0
    
    inFoV = np.where( (xp>xminField) & (xp<=xmaxField) &  (yp>yminField) & (yp<=ymaxField))[0]
    obj = obj[inFoV] ; dat=dat[inFoV]
    
    
    
    
    ##**
    class_starThresh=0.8 # not obvious from 0035 hist.! ***
#    okgal = np.where(dat['star']<class_starThresh)[0] #*** 
    
    
    gal = obj#[okgal] # take all objects as gals
    #stars = obj[]***
    

        
    
    dra = (gal.ra - fInfo['grpra'])*np.cos(np.deg2rad(gal.dec))
    ddec = gal.dec - fInfo['grpdec']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['grpz'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
    #    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
    #    del gal
    #    gal = np.copy(tgal)
    #    del tgal
    #adisMpc = disMpc
    gal.disMpc = disMpc # note: not really appended to recarray!
    

    
    
    
    fits.writeto(name+'info.fits',fInfo,clobber=True)
    fits.writeto(name+'gals.fits',gal,clobber=True)



    """
    plt.figure()
    plt.plot(gal.xp,gal.yp,'k,')
    ok=np.where( (gal.disMpc<=1.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
    plt.plot(gal.xp[ok],gal.yp[ok],'ro',alpha=0.5)
    plt.show()
    """

    
    # setup stars:
    try:
        setup = np.where(dat['handcheck']==1)[0]
        print '%s setup star candidates found'%len(setup)
        fits.writeto(name+'setupCands.fits',obj[setup],clobber=True)
    except:
        try:
            setup = np.where(dat['checked']==1)[0]
            print '%s setup star candidates found'%len(setup)
            fits.writeto(name+'setupCands.fits',obj[setup],clobber=True)
        except:
            print 'no setup stars found!'
    
    okst=setup # assume there MUST be some setup stars by now
    
    # region file:
    f=open('%s.reg'%name,'w')
    f.write('# Region file format: DS9 version 4.1\n')
    #    f.write('global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\n')
    f.write("physical\n")
    for kk in range(len(gal)):
        f.write('circle(%.3f,%.3f,%.3f)\n'%(gal.xp[kk],gal.yp[kk],5.0))
    
    for kk in setup:
        f.write('circle(%.3f,%.3f,%.3f) # color={red}\n'%(obj.xp[kk],obj.yp[kk],10.0))
   
    
    f.close()
    
    
    
    
    
    
    
    
    if (fInfo['grpra']<0.0):
        print '*** using field centre as clus centre ***'
        fInfo['grpra'] = fInfo['cra'] ; fInfo['grpdec'] = fInfo['cdec']
    
    
    
    
    
    
    
    
    dra = (gal.ra - fInfo['grpra'])*np.cos(np.deg2rad(gal.dec))
    ddec = gal.dec - fInfo['grpdec']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['grpz'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
    #    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
    #    del gal
    #    gal = np.copy(tgal)
    #    del tgal
    #adisMpc = disMpc
    gal.disMpc = disMpc # note: not really appended to recarray!
    
    
    
    
    #plotClus()
    # initial plot to identify location of cluster in ra,dec and CMD    
    #def plotClus(gal,fInfo):
    col = gal.zmag - gal.irac1 ; mag = gal.irac1 
    #pri = gal.prob
    xp = gal.xp ; yp = gal.yp
    
    plt.figure()
    plt.subplot(121)
    plt.plot(mag,col,'k,')
    if (gal.izph[0]):
        phzWt = getZphWts(gal,fInfo)
    #        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
    ###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05)  & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim))[0]
    ###        plt.plot( mag[ok],col[ok], 'or', alpha=1, label='|zph-grpz|<=0.05')
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3, label=r'|zph-grpz|<=2$\sigma$')
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=1.0) )[0]
        plt.plot( mag[ok],col[ok], 'or',alpha=0.1,label='zph= 0.7--2.0')
    else:
        ok=np.where( (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3,label='r<1.0Mpc')
        ok=np.where( (gal.disMpc<=0.3) & (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'ok', alpha=0.5,label='r<0.3Mpc')
        ok=np.where( (gal.zmag<=zFnt[1]) & (gal.irac1<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.1)
        
    plt.xlabel('[3.6]')
    plt.ylabel('z - [3.6]')
    plt.xlim([17.5,23])
    plt.ylim([-2,6])
    
    
    xxx = np.arange(0,30)

    plt.plot(xxx,zFnt[1]-xxx,'b--')#,label='faint')
    plt.plot(xxx,zBri[1]-xxx,'b:')#,label='bright')
    plt.plot(xxx,zBri[0]-xxx,'b:')
    plt.plot(xxx,np.polyval(blueCoeffs,xxx),'b-')
    
    plt.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r-',lw=2,label='RS',alpha=0.5)
    plt.plot(xxx,np.polyval(redCoeffs,xxx),'r--',label='RS+0.2')
    #    plt.plot(xxx,np.polyval(redLimCoeffs,xxx),'r-',label='RS+0.5')
    plt.legend(numpoints=1,loc='lower left')
 
    
    # absolute selection box:
    plt.axvline(irac1Lim,color='m')
    
    # red limit with irac1 lim:
    
    # red lim with zBri
    zBriCoeffs = np.array( (-1.,zBri[0]) )
    px0,px1 = line_intersect(redCoeffs,zBriCoeffs)
#    print px0,px1
    # zBri with iracBlue
    px2,px3 = line_intersect(zBriCoeffs,blueCoeffs)
#    print px2,px3
    plt.plot( (px0,px2),(px1,px3), 'k-', lw=3 )
    # red lim with zFnt:
    zFntCoeffs = np.array( (-1.,zFnt[1]) )
    px4,px5 = line_intersect(redCoeffs,zFntCoeffs)
    plt.plot( (px0,px4),(px1,px5), 'k-', lw=3 )
    
    # at irac1Lim:
    # intersect redCoeffs:
    px6,px7 = np.array((irac1Lim,zFnt[1]-irac1Lim))
    plt.plot( (px6,px4), (px7,px5), 'k-', lw=3 )
    # intersect blueCoeffs:
    px8,px9 = irac1Lim,np.polyval(blueCoeffs,irac1Lim)
    plt.plot( (px8,px6), (px9,px7), 'k-', lw=3 )
    # blue lim:
    plt.plot( (px8,px2), (px9,px3), 'k-', lw=3 )

#    print xxx[okB]
#    print zBri[0]-xxx[okB]
#    ok=np.where()
    plt.text(21.5,2.25,'Fnt',rotation=-45,verticalalignment='top')
    plt.text(20.5,2.25,'Bri',rotation=0,verticalalignment='top')


    plt.title('%s\nz=%.3f  RSintcpt=%.2f'%(name,fInfo['grpz'],rintcpt))

    
    # spatial:
    plt.subplot(122)
    plt.plot(gal.ra,gal.dec,'k,')
    
    plt.plot(fInfo['grpra'],fInfo['grpdec'],'og',ms=20,alpha=0.3)
    
    if (gal.izph[0]):
        phzWt = getZphWts(gal,fInfo)
    #        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
    ###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
    ###        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=1)
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=5.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.2)
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=5.5) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or',alpha=0.1)
    else:  
        ok=np.where( (gal.disMpc<=5.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.2,label='Fnt')
        ok=np.where( (gal.disMpc<=1.0) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=0.3,label='r<1.0Mpc')
        ok=np.where( (gal.disMpc<=5.5)& (gal.zmag>zBri[0]) & (gal.zmag<=zBri[1])  & (gal.irac1<=irac1Lim) )[0]
        plt.plot( gal.ra[ok],gal.dec[ok], 'ok', alpha=0.3,label='Bright')
    
    
    ok=np.where( np.abs(gal.zs-fInfo['grpz'])<=0.03 )[0]
    if(len(ok)>0):
        plt.plot( gal.ra[ok],gal.dec[ok], 'ob', alpha=0.7,label='zs')
    
    if(len(okst)>0):
        plt.plot(obj.ra[okst],obj.dec[okst],'*g',ms=15,label='setup stars')
        for iii in okst:
            plt.text(obj.ra[iii],obj.dec[iii],str(obj.ID[iii]))
        

    plt.xlabel('ra')
    plt.ylabel('dec')
    plt.legend(numpoints=1)
    
    
    plt.show()
    plt.savefig('%s_diag.png'%name)

    
    
    



