# global parameters for Ham CCDs
import numpy as np


#### need to update for Ham
# global CCD parameters:
ccd_dx=2048./2.0
ccd_xgap=61./2.0 # may be too big from mosaic?
ccd_dy=4068./2.0

ccd_cx=(2.*(ccd_dx+ccd_xgap)+ccd_dx)/2.
ccd_cy=ccd_dy/2.

x0=490  # updated vals from MLB
x1=2650
y0=0
y1=2088
####

pxscale = 0.08*2.0 # GMOS Ham binned        



xpadPix=1.0
ypadPix=1.0




# GMOS field limits:
yminField = np.copy(y0)
ymaxField = np.copy(y1)
xminSField = 0. # spec field #*** update. grating, cenwave specific?
xmaxSField=3133
xminField = np.copy(x0) # imaging field
xmaxField = np.copy(x1)


# ---- Move global mask design parameters here too:
iRadialWt = False

#allocateSchemes = ['first', 'fillfaint', 'rptfnt', 'rptfnt', 'rptfnt'] # Default
#allocateSchemes = ['bandbri', 'fillfaint', 'rptfnt', 'rptfnt', 'rptfnt']
allocateSchemes = ['fillfaint', 'rptfnt', 'rptfnt', 'rptfnt','bandbri','bandbri'] # BandEnd/


nSingleIter=10 # number of iterations to run on each INDIVIDUAL mask (each call to maskdesHam)
nMultiIter=50   # No of iterations to run on each set of nMasks. This is probably most useful number to optimisation
plotFlag=False
#sortScheme='wts'
sortScheme='shuffle'
#sortScheme='shufflewts'

nomsLen = (6.08/pxscale)    # needs to be a whole number of unbinned pixels (i.e. 3.04" = 38 unbinned pixels)
nomsWid = (1.0/pxscale)
nomsTilt = 0.0

#tarsLen = (4.0/pxscale) # check (2"x2" box)
#tarsWid = (2.0/pxscale)

starsLen=np.copy(nomsLen)
starsWid=np.copy(nomsWid)

NSFlag = True 
# shuffle amount needs to be a whole number of unbinned pix. We are working in 2x2 binnned pixels here
#shuffleOffPix = -nomsLen/8.0   # subtract object position from yobj. This assumes shuffle buffer is ABOVE object and nod direction is 
                            # DOWNWARDS by 1/2 actual slit length = **1/4** virtual slit length used here (i.e. 0.75" for 3" real slit
                            # =6" virtual slit.

#shuffleOffPixUnbinned = np.ceil(tshuffleOffPix*2.0)
#huffleOffPix = tshuffleOffPixUnbinned/2.0
# CAREFUL: apply this shift to galaxies only NOT SETUP STARS!!!!

shuffleOffPix=0.0 # make virtual slits same length for setup stars and gals, therefore no offset!

# z magnitude bins
zFnt = np.array((23.5, 24.25))
##zBri = np.array((23.0, 23.5))
zBri = np.array((21.0, 23.5))
#zBri = np.array((20.0, 23.5))
zVBri = np.array((0.0, 23.0))
#zVBri = np.array((20.0, 23.0))
#zVBri = np.array((19.0, 20.0))
#zVFnt = np.array((24.25, 30.0))
zVFnt = np.array((24.25, 24.50))
irac1Lim=22.5
#irac1Lim=21.5
irac1VFnt=23.0


# blue foreground exclusion cut:
slope = -1.5/3.5
intcpt = 2.0 - slope*19.0
blueCoeffs = np.array((slope,intcpt))
#(irac1, z-irac1)

# RS coeffs are now stored in fInfo

dRed=0.2    # first limit past red sequence. Start to downweight gals past here
dRedLim=0.5 # ultimate limit past red-sequence. set weights to zero above this


# GMOS Ham:
# R150, cenwave=850nm, +RG610:
cenwave=850 ; linDisp=0.39353344971414417 ; WLcoeff=np.array([-7.78878524e-02,   1.54497292e+02])
blueCutA = 610.0
redCutA = 1045.0

